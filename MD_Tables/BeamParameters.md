Section  |      Parameter |     Value | Unit
-------- |--------------- |---------- |-----
LEBT     |                |           |     
&nbsp;   |         energy |     0.075 |  MeV
&nbsp;   |        current |        74 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.14418 |mm.mrad
&nbsp;   |  emittance yyp |   0.14404 |mm.mrad
&nbsp;   |  emittance zzp |        -0 |mm.mrad
&nbsp;   |   twiss alphax |   -4.2426 |     
&nbsp;   |    twiss betax |   0.43525 |mm/mrad
&nbsp;   |   twiss alphay |   -4.2463 |     
&nbsp;   |    twiss betay |   0.43564 |mm/mrad
&nbsp;   |   twiss alphaz |         0 |     
&nbsp;   |    twiss betaz |    96.956 |mm/mrad
RFQ      |                |           |     
&nbsp;   |         energy |  0.074969 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |        74 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.12909 |mm.mrad
&nbsp;   |  emittance yyp |    0.1296 |mm.mrad
&nbsp;   |  emittance zzp |        -0 |mm.mrad
&nbsp;   |   twiss alphax |    1.6632 |     
&nbsp;   |    twiss betax |   0.19471 |mm/mrad
&nbsp;   |   twiss alphay |     1.666 |     
&nbsp;   |    twiss betay |   0.19421 |mm/mrad
&nbsp;   |   twiss alphaz |  -0.19123 |     
&nbsp;   |    twiss betaz |    9.6877 |mm/mrad
MEBT     |                |           |     
&nbsp;   |         energy |    3.6082 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |      0.25 |mm.mrad
&nbsp;   |  emittance yyp |      0.25 |mm.mrad
&nbsp;   |  emittance zzp |   0.25821 |mm.mrad
&nbsp;   |   twiss alphax |  -0.23341 |     
&nbsp;   |    twiss betax |   0.29431 |mm/mrad
&nbsp;   |   twiss alphay |  -0.66313 |     
&nbsp;   |    twiss betay |   0.52141 |mm/mrad
&nbsp;   |   twiss alphaz | -0.056475 |     
&nbsp;   |    twiss betaz |    1.4581 |mm/mrad
DTL      |                |           |     
&nbsp;   |         energy |      3.62 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.27994 |mm.mrad
&nbsp;   |  emittance yyp |   0.29695 |mm.mrad
&nbsp;   |  emittance zzp |   0.30381 |mm.mrad
&nbsp;   |   twiss alphax |    1.3068 |     
&nbsp;   |    twiss betax |   0.14831 |mm/mrad
&nbsp;   |   twiss alphay |   -3.7227 |     
&nbsp;   |    twiss betay |   0.84605 |mm/mrad
&nbsp;   |   twiss alphaz |   0.58092 |     
&nbsp;   |    twiss betaz |   0.47579 |mm/mrad
SPK      |                |           |     
&nbsp;   |         energy |    89.892 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.40792 |mm.mrad
&nbsp;   |  emittance yyp |   0.40341 |mm.mrad
&nbsp;   |  emittance zzp |   0.40139 |mm.mrad
&nbsp;   |   twiss alphax |   -2.5378 |     
&nbsp;   |    twiss betax |     5.708 |mm/mrad
&nbsp;   |   twiss alphay |   0.89397 |     
&nbsp;   |    twiss betay |    3.5097 |mm/mrad
&nbsp;   |   twiss alphaz |   0.23326 |     
&nbsp;   |    twiss betaz |    6.5884 |mm/mrad
MBL      |                |           |     
&nbsp;   |         energy |    216.54 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.48006 |mm.mrad
&nbsp;   |  emittance yyp |   0.43793 |mm.mrad
&nbsp;   |  emittance zzp |   0.42231 |mm.mrad
&nbsp;   |   twiss alphax |   -1.9496 |     
&nbsp;   |    twiss betax |    11.483 |mm/mrad
&nbsp;   |   twiss alphay |  -0.16775 |     
&nbsp;   |    twiss betay |    6.7878 |mm/mrad
&nbsp;   |   twiss alphaz |   0.52408 |     
&nbsp;   |    twiss betaz |    10.235 |mm/mrad
HBL      |                |           |     
&nbsp;   |         energy |    570.96 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.51768 |mm.mrad
&nbsp;   |  emittance yyp |   0.43573 |mm.mrad
&nbsp;   |  emittance zzp |   0.44712 |mm.mrad
&nbsp;   |   twiss alphax |   -2.8732 |     
&nbsp;   |    twiss betax |    20.465 |mm/mrad
&nbsp;   |   twiss alphay |  -0.38354 |     
&nbsp;   |    twiss betay |    7.2336 |mm/mrad
&nbsp;   |   twiss alphaz |   0.32553 |     
&nbsp;   |    twiss betaz |    14.985 |mm/mrad
HEBT     |                |           |     
&nbsp;   |         energy |    1991.1 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.52579 |mm.mrad
&nbsp;   |  emittance yyp |   0.46982 |mm.mrad
&nbsp;   |  emittance zzp |   0.46293 |mm.mrad
&nbsp;   |   twiss alphax |   -1.4364 |     
&nbsp;   |    twiss betax |    40.586 |mm/mrad
&nbsp;   |   twiss alphay |   0.40756 |     
&nbsp;   |    twiss betay |    23.538 |mm/mrad
&nbsp;   |   twiss alphaz |   0.15223 |     
&nbsp;   |    twiss betaz |    49.197 |mm/mrad
A2T      |                |           |     
&nbsp;   |         energy |    1991.1 |  MeV
&nbsp;   |      frequency |    352.21 |  MHz
&nbsp;   |        current |      62.5 |   mA
&nbsp;   |     duty cycle |         4 |    %
&nbsp;   |  emittance xxp |   0.48779 |mm.mrad
&nbsp;   |  emittance yyp |   0.44815 |mm.mrad
&nbsp;   |  emittance zzp |   0.76167 |mm.mrad
&nbsp;   |   twiss alphax |  -0.93743 |     
&nbsp;   |    twiss betax |    29.689 |mm/mrad
&nbsp;   |   twiss alphay |   0.45468 |     
&nbsp;   |    twiss betay |    10.782 |mm/mrad
&nbsp;   |   twiss alphaz |    -14.56 |     
&nbsp;   |    twiss betaz |    1541.2 |mm/mrad
