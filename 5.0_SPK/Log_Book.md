LEDP
----

Date        Modifier        What has changed?
*********************************************

2013-Sep-23 Mamad           The folders are created.
2013-Oct-16 Mamad           A layout of LEDP as proposed by Peter is added to drawing folder

Spoke
-----

Date        Modifier        What has changed?
*********************************************
2013-Sep-23 Mamad           The folders are created.
2013-Oct-07 S.Molloy        Added CST & STP model of spoke cav/cm + a pretty JPG.
2013-Oct-16 mamad           The new lattice has longer LWUs due to vacuum and mechanical considerations.
2013-Oct-16 Mamad           The Input beam and the Spoke lattice (including the LEDP) is added.
2016-Feb-02 Mamad 	  Removed the technical drawings folders as they could get out of sync with reality