Date		Modifier		What has changed?
2013-Sep-23	Mamad			The folders are created.
2013-Oct-17	Heine			Beam physics files (raster V24) added.	
2014-Jul-03	Heine			HEBT V27 (->TGT + ->DMPL) added.
					This was used in IPAC'14
					papers + recent error studies
2014-Oct-20	Heine			HEBT revision number was
					promoted to 28 to distinguish
					it from v.27 with short dogleg
					LWUs. raster27_long_DGLG_LWUs
					is thus completely identical
					to HEBT_raster28. 
2014-Nov-19	Heine			Raster29: revised beam on
					target parameters: smaller
					beamlet to compensate for
					smaller tolerable horizontal
					BEW footprint.
2015-Jan-28	Heine			Updated to raster3X
					Renumbering of MATCH and DIAG
					commands
					Implemented relative element
					numbers in a few matching
					commands
					Set all magnet strengths in .dat to nominal values

Notes:
- DIAG_POSITION 89099 is used to deflect beam centroid to raster
  pattern amplitude

