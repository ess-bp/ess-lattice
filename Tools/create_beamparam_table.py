from ess import TraceWin

# Verify that parameters
# that should be 0 are 0
VERIFY_ZERO = True

# Verify paths are correct in file
VERIFY_PATHS = True


def print_line(s, p, v, u):
    if isinstance(v, float):
        v = f"{v:.5g}"
    if s == "":
        s = "&nbsp;"
    print(f"{s:<8} |{p:>15} |{v:>10} |{u:>5}")


def print_hline():
    print_line("-" * 8, "-" * 15, "-" * 10, "-" * 5)


print_line("Section", "Parameter", "Value", "Unit")
print_hline()

units = {"beta": "mm/mrad", "emit": "mm.mrad", "energy": "MeV", "curr": "mA", "freq": "MHz", "dcycle": "%"}
factors = {"emit": 1e6, "energy": 1e-6, "curr": 1e3}
replaces = {"emit_": "emittance ", "twiss_": "twiss ", "dcycle": "duty cycle", "curr": "current", "freq": "frequency"}
ignores = {"LEBT": ["freq"]}

for section in ["LEBT", "RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T"]:
    project = TraceWin.project(f"ProjectFiles/{section}.ini")
    print_line(section, "", "", "")
    for key in [
        "energy",
        "freq",
        "curr",
        "dcycle",
        "emit_xxp",
        "emit_yyp",
        "emit_zzp",
        "twiss_alphax",
        "twiss_betax",
        "twiss_alphay",
        "twiss_betay",
        "twiss_alphaz",
        "twiss_betaz",
    ]:
        if section in ignores and key in ignores[section]:
            continue
        value = project.get("main:beam1_" + key)
        unit = ""
        for factor in factors:
            if factor in key:
                value *= factors[factor]
        for u in units:
            if u in key:
                unit = units[u]
        for r in replaces:
            key = key.replace(r, replaces[r])
        print_line("", key, value, unit)

    if VERIFY_ZERO:
        for key in [
            "rms_energy_spread",
            "twiss_mismatchX",
            "twiss_mismatchY",
            "twiss_mismatchZ",
            "twiss_bcenter_dx",
            "twiss_bcenter_dxp",
            "twiss_bcenter_dy",
            "twiss_bcenter_dyp",
            "twiss_bcenter_dz",
            "twiss_bcenter_dzp",
        ]:
            value = project.get("main:beam1_" + key)
            if value != 0:
                raise ValueError(f"{key} = {value} in project file {section}.ini, expected 0")

    if VERIFY_PATHS:
        calc_dir = project.get("main:calc_dir")
        calc_dir_correct = f"calc_{section.lower()}"
        if calc_dir != calc_dir_correct:
            raise ValueError(f"Calculation directory in {section}.ini is {calc_dir}, should be {calc_dir_correct}")
