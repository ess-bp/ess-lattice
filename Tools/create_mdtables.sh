#!/bin/bash

# This script use csvtomd, from https://github.com/mplewis/csvtomd
# If you don't have it on your system, run
# pip3 install csvtomd

echo "Looking for csvtomd"
command -v csvtomd >/dev/null 2>&1 || { echo >&2 "ERROR: Need script csvtomd to update MD tables"; exit 1; }

for f in elements.csv sections.csv subsections.csv
do
    echo $f
    echo -e "[link to csv file](../CSV_Tables/$f)\n" > MD_Tables/${f%.*}.md
    csvtomd CSV_Tables/$f >> MD_Tables/${f%.*}.md
    python Tools/create_beamparam_table.py > MD_Tables/BeamParameters.md
done
