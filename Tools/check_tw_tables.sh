#!/bin/bash

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     TRACEWIN=tracelx64;;
    Darwin*)    TRACEWIN=tracemac64;;
    *)          TRACEWIN="UNKNOWN:${unameOut}"
esac

echo "Using $TRACEWIN"

# Make a soft link to the binary if not already existing
if [[ ! -f $TRACEWIN ]]
then
    if [[ ! -f ~/TraceWin/exe/$TRACEWIN ]]
    then
        echo "ERROR, file ~/TraceWin/exe/$TRACEWIN not found"
        exit 1
    elif [[ ! -x ~/TraceWin/exe/$TRACEWIN ]]
    then
        echo "ERROR, file ~/TraceWin/exe/$TRACEWIN is not executable"
        exit 1
    fi
    ln -s ~/TraceWin/exe/$TRACEWIN
fi


print_usage() {
    echo "$1 [-u]"
    echo "   Default is to compare tables, use argument -u to update tables instead"
}

update_files() {
    cp ProjectFiles/LEBT.ini .
    python Tools/merger.py
    ./$TRACEWIN LEBT.ini partran=0 synoptic_file=2 hide_esc
    mv geometric_layout_middle_pos.dat MD_Tables/LEBT_TGT_Geo_mid_point.dat
    python Tools/merger.py -r A2T -a DMPL
    ./$TRACEWIN LEBT.ini partran=0 synoptic_file=2 hide_esc
    mv geometric_layout_middle_pos.dat MD_Tables/LEBT_DMPL_Geo_mid_point.dat
    python Tools/merger.py
}

while getopts :uh opt
do
    case $opt in
        u) update_files
           exit 0;;
        h) print_usage $0
           exit 0;;
        ?) print_usage $0
           exit 1;;
    esac
done

# This is the default operation..
cp ProjectFiles/LEBT.ini .
python Tools/merger.py
./$TRACEWIN LEBT.ini partran=0 synoptic_file=2 hide_esc
diff -q geometric_layout_middle_pos.dat MD_Tables/LEBT_TGT_Geo_mid_point.dat || exit $?
rm geometric_layout_middle_pos.dat
python Tools/merger.py -r A2T -a DMPL
./$TRACEWIN LEBT.ini partran=0 synoptic_file=2 hide_esc
diff -q geometric_layout_middle_pos.dat MD_Tables/LEBT_DMPL_Geo_mid_point.dat || exit $?
rm geometric_layout_middle_pos.dat
python Tools/merger.py
