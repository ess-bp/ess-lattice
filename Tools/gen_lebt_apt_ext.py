"""
    A 184.1 mm part of the 254.1 mm extraction.

    - 50 mm to 75 mm in 183.1 mm.
    - Flat 75 mm for the last 1 mm.
"""

# --

import numpy as np
import sys

# --

slope = (75.0 - 50.0) / 183.1

s = np.linspace(0.0, 183.1, 201)
apt = 50.0 + slope * s

# 1 mm plateau
s = np.append(s, 184.1)
apt = np.append(apt, 75.0)

# --

s *= 1e-3
apt *= 1e-3

# --

with open("LEBT_Apt_Ext.ouv", "w") as file:
    file.write("{:d}\n".format(len(s)))
    for i in np.arange(len(s)):
        file.write("{:.7f}  {:.6f}\n".format(s[i], apt[i]))

# --

sys.exit()

# --
