dest="TARGET"
echo "Generating CSV for $dest"

python Tools/merger.py

if [ ! -d "tmp/$dest" ];
then
	mkdir "tmp/$dest"
fi
python Tools/tracewin2csv.py $dest tmp/$dest lattice.dat
