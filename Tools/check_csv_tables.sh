python Tools/tracewin2csv.py -f . -e CSV_Tables/PBI/*.csv

for f in CSV_Tables/*.csv
do
    file=$(basename "$f")
    echo "Checking $f"
    diff $file $f || exit $?
done

rm elements.csv sections.csv subsections.csv
python Tools/merger.py
