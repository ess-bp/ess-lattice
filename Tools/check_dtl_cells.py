import numpy as np
import pandas as pd

"""
This script programatically checks the DTL lattice against the Excel file with tables for each tank

To use, copy ESS-0103355 to the top level folder, and run

python Tools/check_dtl_cells.py > DTL_lattice.dat

"""
# Reading in sheets from excel
stem_axis = []
cell_length = []
end = [63, 36, 31, 28, 25]
for j in range(5):
    t = pd.read_excel("ESS-0103355_DTL_V.86_All_Parameters_2017_02_07_BPM-pos.xlsx", "tank{}".format(j + 1))
    stem_axis.append(np.array(t["Stem Axis"][2 : end[j]]))
    cell_length.append(np.array(t["Cell Length"][2 : end[j]]))

# integrated length of DTL_CEL
z0 = 0.0
# cell counter
i = 0
# tank counter
j = 0


def print_replace_z(line, z):
    lsp = line.split("=")
    print(lsp[0] + "={:.5f}]".format(z))


for f in open("4.0_DTL/Beam_Physics/lattice.dat", "r"):
    fsp = f.split()
    if len(fsp) > 1:
        if fsp[0] == "DTL_CEL":
            print(f.strip())
            if i == -1:
                i += 1
                continue
            c_len = float(fsp[1])
            z0 += c_len
            cl = cell_length[j][i]
            # if abs(c_len-cl)>1e-4:
            #    print(';WARNING, Tank {}, cell {}, TW cell length: {}, Table cell length: {:.4f}'.format(j,i,c_len,cl))
            i += 1
        elif fsp[0] in ["DIAG_POSITION", "THIN_STEERING"]:
            print_replace_z(f, stem_axis[j][i] - z0)
        else:
            print(f.strip())
    else:
        print(f.strip())

    if i == len(stem_axis[j]) and j < 4:
        # print("; Length of tank: {}, number of elements: {}".format(z0,i))
        j += 1
        i = -1
        z0 = 0.0


# print("; Length of tank: {}, number of elements: {}".format(z0,i))
