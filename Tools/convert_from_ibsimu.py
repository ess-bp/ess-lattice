from ess import TraceWin
import numpy as np

fname = "/storage/oysteinmidttun/simulations/ps-ess/2d/005/tracewin_path_format.txt"
data = np.loadtxt(fname, skiprows=8)
tot = data.shape[0]
tot_charges = sum(data[:, 8])

# get header data..
tot_current = 0.0
read_data = 0
for fline in open(fname, "r"):
    if "Total beam:" in fline:
        tot_current = float(fline.split()[2])
        read_data += 1
    if read_data > 0:
        break

print("Total current:", tot_current)
print("Total number of particles", tot)


# We do not yet have a way to produce an empty dst object..
dst = TraceWin.dst("part_dtl1.dst")
dst.remove()

p0 = 0.075

# filter on mass and charge..
data = data[np.where(data[:, 9] == 9.314940136e-01)]
data = data[np.where(data[:, 8] == 1)]

x = data[:, 1]
xp = data[:, 2]
y = data[:, 3]
yp = data[:, 4]
pz = data[:, 6] * p0 + p0
# mass=data[:,9]
# charge=data[:,8]


for i in range(len(x)):
    # NOTE, we are here adding momentum as equal to energy, not true!!
    dst.append(x=x[i], xp=xp[i], y=y[i], yp=yp[i], E=pz[i])

print("Number of protons found {}, {} %".format(dst.Np, dst.Np / tot * 100))

dst.Ib = tot_current * 1e3 * sum(data[:, 8]) / tot_charges

dst.save("ibsimu_protons.dst")

# Plotting, can be skipped..

from matplotlib import pyplot as plt

plt.figure(figsize=(13, 12))
data = TraceWin.dst("ibsimu_protons.dst")
data.subplot(221, "x", "xp")
data.subplot(222, "y", "yp")
data.subplot(223, "E")
data.subplot(224, "x", "y")

plt.show()
# plt.savefig('ibsimu_dst.png')
