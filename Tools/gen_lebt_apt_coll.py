"""
    Aperture for the second half of the collimator.

    - 35 to 7 mm over 173 mm within the collimator.
"""

# -- Inputs

l0 = 173.0  # Total length of the slope
l1 = 86.5  # Starting point within the slope
l2 = l0 - l1  # Generate a map for this part
ds = 173.0 / 200.0  # Step size

# --

import numpy as np

# --

s = np.linspace(0.0, l2 // ds * ds, l2 // ds + 1)
apt = 35.0 - 28.0 / 173.0 * (l1 + s)

if s[-1] != l2:
    s = np.append(s, l2)
    apt = np.append(apt, 35.0 - 28.0 / 173.0 * l0)

# --

s = np.append(s, s[-1] + 2.0)
apt = np.append(apt, apt[-1])

# --

s *= 1e-3
apt *= 1e-3

# --

with open("LEBT_Apt_Coll.ouv", "w") as file:
    file.write("{:d}\n".format(len(s)))
    for i in np.arange(len(s)):
        file.write("{:.6f}  {:.5f}\n".format(s[i], apt[i]))
