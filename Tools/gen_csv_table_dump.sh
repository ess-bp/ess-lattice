dest="DUMP"
echo "Generating CSV for $dest"

python Tools/merger.py -r A2T -a DMPL

if [ ! -d "tmp/$dest" ];
then
	mkdir "tmp/$dest"
fi
python Tools/tracewin2csv.py $dest tmp/$dest lattice.dat
