# Installation

To build manually, see TraceWin documentation

To build with cmake, run

```
cd Tools/extraElements
mkdir build
cd build
cmake ..
make install
```

The generated library `my_elements.so` (Linux) or `my_elements.dylib` (OSX) needs to be in the running folder or in the same folder as the binary, so place it as appropriate if need be.


# Elements


## Raster

The rastering elements will simulate the beam being rastered onto the target. You first need a `clear_raster` element befor any raster commands, and then you put in the raster command at each rastering magnet.

The rastering is done with a perfect sawtooth signal, the low-pass filter is not included.

Note that this will mess up the space-charge calculation, as each particle of your distribution is now assigned a randomized rastering amplitude. For areas such as the cross-over where rastering is cancelled out, this should be less problematic.

These commands have no effect for envelope mode. The raster element is treated as a drift space if you have set a non-zero length.

### MY\_ELE(clear\_raster) LENGTH APERTURE NSTEP PULSE\LENGTH

LENGTH should be set to 0, APERTURE and NSTEP is ignored for this command. PULSE\_LENGTH is needed to calculate how many buckets exist for the pulse in total.

The command will assign a random bucket for each particle in the distribution, and write down this assignment in the file `bucket_list.txt` in the calculation folder.

### MY\_ELE(raster) LENGTH APERTURE NSTEP FREQ AMPL PLANE

LENGTH should be set to 0. For non-zero length a drift is calculated before the kick, but this has never been used/debugged.

APERTURE should be set to the aperture of the element.

NSTEP is the number of steps you should use. 1 Recommended.

FREQ is the frequency of this rastering magnet in Hz.

AMPL is the amplitude of the kick, in Tesla-metres. Recommend to use positive amplitude but negative should also work. If you have a `THIN_STEERING` magnet for this, you use the same kick calculated for this one (see lattice.cal)

PLANE The plane of this rastering magnet, 1 for horizontal or 2 for vertical.

