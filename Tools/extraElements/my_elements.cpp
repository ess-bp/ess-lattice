/***************************************************************************
main.cpp
Windows -> Dynamic library (dll)
Linux or MacOS -> Dynamic shared object (so, dylib)
-------------------
copyright : (C) 2021 by European Spallation Source ERIC
email : yngve.levinsen@ess.eu
***************************************************************************/

#ifdef _WIN32
  #include <windows.h>
  #define DLL_EXPORT __declspec(dllexport)
#else
  #define DLL_EXPORT
#endif
#define SPEED_OF_LIGHT 299792458.0

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>

#ifdef __cplusplus
extern "C" {
#endif

int DLL_EXPORT raster_partran(double Zs,double *param,int Nele,int npart,double *cord,double *loss,double freq,double mass0,int q,double *ws,double *Ibeam,double *extra_param,char *error_mess)

{

  double x,y,xp,yp,z,w,dpsp,r,zzs,bgs,gamma,gams,betas,ds,Aperture, rasterFreq, rasterKick;
  int rasterIndex;
  int Nstep;

  //
  // Zs          :  Current position in the element (from 0 to Length-Length/Nstep)
  // param[1]    : Length/Nstep (m)
  // param[2]    : Aperture (m)
  // param[3]    : Nstep
  // param[4]    : rastering frequency [Hz]
  // param[5]    : rastering amplitude B*L [Tm]
  // param[6]    : Orientation, 1=H, 2=V
  // cord        : See example
  // loss        : see example
  // freq        : beam frequency (Hz)
  // mass0        : Particle mass (eV)
  // q           : particle charge state
  // *ws         : reference kinetic energy (eV) (can be modified)
  // *Ibeam      : Beam current (A) (can be modified)
  // error_mess  : TraceWin stop and show this error message if this function return 0
  // if error_mess!="" and function return 1, this message is print to the standard console without stop TraceWin

  ds=param[1];
  Aperture=param[2];
  Nstep = param[3];


  strcpy(error_mess, "");
  zzs = (*ws) / mass0;
  bgs = sqrt(zzs * (2. + zzs));
  gams = 1.0 + zzs;
  betas = bgs / gams;

  rasterFreq = param[4];
  rasterKick = param[5] * q / mass0 / betas / gams * SPEED_OF_LIGHT;
  rasterIndex = 2 * param[6] - 1; // 1 for H, 3 for V

  int bucket = -1;
  double buck;
  std::ifstream myfile ("bucket_list.txt");
  int nbuckets;
  myfile >> nbuckets;
  double nx, period, signal;

  for (int i=0;i<npart;i++) {
    if ((int)loss[i]==0) {
      x = cord[i*6];      // m
      xp = cord[i*6+1];   // rad
      y = cord[i*6+2];    // m
      yp = cord[i*6+3];   // rad
      z = cord[i*6+4];    // m
      dpsp = cord[i*6+5]; // dp/p
      r = sqrt(x*x+y*y);
      if (r>Aperture) loss[i] = Nele; // A particle lost has to be set to Element number
      else {
        x=x+ds*xp;
        y=y+ds*yp;
        w=dpsp*betas*betas*gams*mass0+(*ws);
        gamma = 1+w/mass0;
        z=z+dpsp*ds/(gamma*gamma);
      }
      cord[i*6]   = x;
      cord[i*6+2] = y;
      cord[i*6+4] = z;
      // TODO: No low pass filter here yet, this is a perfect sawtooth..
      if (myfile.is_open() && (std::abs(rasterKick) > 1e-12)) {
          myfile >> bucket;
          buck = bucket * 1.0 / nbuckets; // fraction
          nx = freq / rasterFreq;
          period = nbuckets / nx;
          signal = std::modf(buck * period, &buck);
          if (signal > 0.5) signal = 1.0 - signal;
          signal = 4 * (signal - 0.25);
          cord[i*6 + rasterIndex] += rasterKick / Nstep * signal;
      }
    }
  }
  myfile.close();
  return(1);
}


// This is just a drift
int DLL_EXPORT raster_envelope(double Zs,double *param,int Nele,double Tmat[][6],double *Bcent,double freq,double mass0,int q,double *ws,double *ibeam,double *extra_param,char *error_mess)
{
  //
  // Zs          :  Current position in the element (from 0 to Length-Length/Istep)
  //                my_ele_envelope is first time called wiht parma[1]=Length (in this case Zs=-10)
  //                the following calls is performed Istep times for Zs from 0 to Length-Length/Istep (Istep is defined by TraceWin according to "step of calcul" parameter of "Main" page.
  // param[1]    : Length(m) or Length/Istep (m)
  // param[2]    : Aperture (m)
  // param[3]    : Nstep
  // cord        : See example
  // loss        : see example
  // freq        : beam frequency (Hz)
  // mass        : Particle mass (eV)
  // q           : particle charge
  // *ws         : reference kinetic energy (eV) (can be modified)
  // *Ibeam      : Beam current (A) (can be modified)
  // error_mess  : TraceWin stop and show this error message if this function return 0
  // if error_mess!="" and function return 1, this message is print to the standart console without stop TraceWin

  double gams;
  double ds = param[1];
  strcpy(error_mess, "");

  gams = 1 + (*ws) / (mass0);
  Tmat[0][0] = Tmat[1][1] = Tmat[2][2] = Tmat[3][3] = Tmat[4][4] = Tmat[5][5] = 1;
  Tmat[0][1] = Tmat[2][3] = ds;
  Tmat[4][5] = ds / (gams * gams);

  return(1);

}


int DLL_EXPORT clear_raster_partran(double Zs,double *param,int Nele,int npart,double *cord,double *loss,double freq,double mass0,int q,double *ws,double *Ibeam,double *extra_param,char *error_mess)
{
  // Generate file bucket_list.txt with a bucket index number randomly generated for each particle.
  // Set this element before any raster magnets
  // param[1]    : Length/Nstep (m)
  // param[2]    : Aperture (m)
  // param[3]    : Nstep
  // param[4]    : pulse length [Hz]

  double tau;

  tau = param[4];

  // Assume bucket list exists..
  std::ofstream myfile;
  myfile.open("bucket_list.txt");
  int nbuckets = freq  * tau;
  myfile <<  nbuckets << "\n";
  for (int i=0;i<npart;i++)
    myfile << (rand() % nbuckets) << "\n";
  myfile.close();
  return(1);

}

int DLL_EXPORT clear_raster_envelope(double Zs,double *param,int Nele,double Tmat[][6],double *Bcent,double freq,double mass0,int q,double *ws,double *ibeam,double *extra_param,char *error_mess)
{
  // empty function

  return(1);

}

#ifdef __cplusplus
} // "C"
#endif
