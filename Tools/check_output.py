"""
Used to check output generated for a given part of lattice
"""


def _compare_files(f1, f2, cfg=None):
    import sys

    if cfg:
        import subprocess

        try:
            stdout = subprocess.check_output("ndiff {} {} {}".format(f1, f2, cfg), shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as error:
            print("Error running command...")
            print(error.cmd)
            print(error.output)
            sys.exit(error.returncode)
        if b"do not differ" in stdout:
            print(f2, 1.0)
            return 1.0
        else:
            print(stdout.decode("utf-8"))
            return 0.0
    else:
        import difflib

        result = difflib.SequenceMatcher(None, open(f1, "r").read(), open(f2, "r").read())
        print(f2, result.ratio())
        return result.ratio()


def _check_correct_output(fpath, fpath_correct):
    errors = []
    if not os.path.isdir(fpath):
        print("Missing calculation folder")
    if not os.path.isdir(fpath_correct):
        print("Missing reference calculation folder")

    for f in os.listdir(fpath_correct):
        if f.split(".")[-1] == "cfg":
            continue

        f_ref = os.path.join(fpath_correct, f)
        f_new = os.path.join(fpath, f)
        f_cfg = f_ref[:-3] + "cfg"

        if os.path.isfile(f_cfg):
            ratio = _compare_files(f_ref, f_new, f_cfg)
        else:
            ratio = _compare_files(f_ref, f_new)
        if ratio < 0.97:  # silly simple comparison for now..
            errors.append([f_new, ratio])
    if errors:
        for e in errors:
            print(f"Ratio {e[1]} too large for {e[0]} ")
        raise ValueError(f"There were {len(errors)} files failing the comparison test")


if __name__ == "__main__":
    import sys
    import os

    print("Will check output files in folder", sys.argv[1])

    f1 = os.path.join(sys.argv[1], "Beam_Physics", "OutputFiles")
    if len(sys.argv) > 2:
        f2 = sys.argv[2]
    else:
        f2 = "."
    _check_correct_output(f2, f1)
