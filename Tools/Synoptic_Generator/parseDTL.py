from parsebase import parsebase


class DTL(parsebase):
    def __init__(self):
        parsebase.__init__(self, "DTL")
        self.inDTL = False
        self.current_DTL = 1

    def append(
        self,
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    ):
        # First deal with DT starts..
        if element == "QV" and index == 1 and not self.inDTL:
            self.inDTL = True
            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
                id="DTC Start",
            ):
                self.doc.stag("img", klass="element_img", src=self.dict["DTC_Start"]["src"])

        if element in (
            "QV",
            "QH",
            "BLM",
            "CV",
            "CH",
            "BPM",
            "NPM",
            "FC",
            "BCM",
            "LBM",
            "BLM",
        ):
            if model and model in self.dict[element]:
                base = self.dict[element][model]
            else:
                base = self.dict[element]
            if slot_type and slot_type in base:
                base = base[slot_type]

            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                slot_type=slot_type,
                id=element,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
            ):
                self.doc.stag("img", klass="element_img", src=base["src"])

        elif element == "DTC":
            if index == 1 and self.current_DTL > 1:
                self.inDTL = True
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname=essname,
                    model=model,
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id="DTC Start",
                ):
                    self.doc.stag("img", klass="element_img", src=self.dict["DTC_Start"]["src"])
            elif index == self.dict["DTC_End"]["ncells"][self.current_DTL]:
                self.inDTL = False
                self.current_DTL += 1
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname=essname,
                    model=model,
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id="DTC End",
                ):
                    self.doc.stag("img", klass="element_img", src=self.dict["DTC_End"]["src"])

        elif element not in ["Drf"]:
            # Print ignored elements (to check we are taking all)
            print("Ignoring element", element, "in DTL")
