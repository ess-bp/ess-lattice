from parsebase import parsebase


class MEBT(parsebase):
    def __init__(self):
        parsebase.__init__(self, "MEBT")
        self.filebase = "RFQ-MEBT"

    def append(
        self,
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    ):
        # Add the RFQ block:
        if element == "BCM" and section == "RFQ":
            if section == "RFQ":
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname=essname,
                    model=model,
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id="RFQ",
                ):
                    self.doc.stag("img", klass="element_img", src=self.dict["RFQ"]["src"])

        if element in [
            "Grid",
            "QV",
            "QH",
            "CX",
            "BPM",
            "Chop",
            "ChDump",
            "WS",
            "Cav",
            "Coll",
            "BLM",
            "NPM",
            "EMU",
            "LBM",
            "FC",
            "BCM",
        ]:
            if model:
                base = self.dict[element][model]
            else:
                base = self.dict[element]
            if slot_type and slot_type in base:
                base = base[slot_type]

            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                model=model,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
                id=element,
            ):
                self.doc.stag("img", klass="element_img", src=base["src"])

        elif element not in ["Drf", "RFC"]:
            # Print ignored elements (to check we are taking all)
            print("Ignoring element", element, "in MEBT")
