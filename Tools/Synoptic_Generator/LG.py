import parseLEBT
import parseMEBT
import parseSPK
import parseMBL
import parseHBL
import parseHEBT
import parseA2T
import parseDTL
import parseDMPL
import parsetools

import csv
import sys
import os
import numpy as np
from ess import TraceWin

# CLEAN THE HTML FILES
for f in ["LEBT", "RFQ-MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T", "DMPL"]:
    parsetools.makedirs(os.path.join("HTML", f), exist_ok=True)
    open(os.path.join("HTML", f, f + ".html"), "w").close()

# Allow first argument to be path to csv file
if len(sys.argv) > 1:
    ELEMENTS_CSV = sys.argv[1]
    ELEMENTS_CSV_DMPL = sys.argv[2]
else:
    folder = "CSV_Tables"
    for i in range(3):
        if os.path.exists(folder):
            break
        folder = os.path.join("..", folder)
    ELEMENTS_CSV = os.path.join(folder, "elements.csv")

# OPEN THE TARGET CSV AND SPIT THE COLUMNS INTO PYTHON LISTS
with open(ELEMENTS_CSV) as csvfile:
    rows = csv.reader(csvfile, delimiter=",", quotechar='"')
    headers = next(rows)

    res = list(zip(*rows))

    # First and last line should be removed,
    # we make it a bit more generic (in case of future changes)
    removes = []
    for i in range(len(res[0])):
        if res[0][i] == "":
            removes.append(i)
    removes.sort(reverse=True)
    content = {}
    for h, r in zip(headers, res):
        r2 = list(r)
        for i in removes:
            del r2[i]
        content[h] = r2

section = list(content["sect"])
slot_number = list(content["cell"])
slot_type = list(content["slot"])
model = list(content["model"])
element = list(content["elem"])
index = list(map(int, content["index"]))
essname = list(content["essname"])
aperture = list(map(float, content["r [mm]"]))
TCSz = list(map(float, content["TCS:m:z [mm]"]))
TCSy = list(map(float, content["TCS:m:y [mm]"]))
BPSz = list(map(float, content["BPS:m:z [mm]"]))
BPSy = list(map(float, content["BPS:m:y [mm]"]))

# BEGIN ENERGY CODE

# Stack all tracewin output tables (until 2 GeV):
z = 0
fcount = 0
energyZ = []
energy = []

for s in ["LEBT", "RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL"]:
    fcount += 1
    tfile = f"{fcount}.0_{s}/Beam_Physics/OutputFiles/tracewin.out"
    pfile = f"{fcount}.0_{s}/Beam_Physics/OutputFiles/partran1.out"
    if os.path.isfile(pfile):
        data = TraceWin.partran(pfile)
    else:
        data = TraceWin.partran(tfile)
    energyZ.extend(data["z(m)"] * 1e3 + z)
    z = energyZ[-1]
    energy.extend(data["gama-1"] * 938.272029)

energy = np.array(energy)
energyZ = np.array(energyZ)


def get_energy_at(z):
    return np.interp(z, energyZ, energy)


elementEnergy = [""] * len(BPSz)
for i in range(len(BPSz)):
    elementEnergy[i] = get_energy_at(BPSz[i])

# END ENERGY CODE

# BEGIN PBI_SHOPPINGLIST_LINKS
with open(os.path.join(os.path.dirname(sys.argv[0]), "shoppingList.csv")) as csvfile:
    rows = csv.reader(csvfile, delimiter=",")
    res = list(zip(*rows))

insight_ESSName = res[0]
insight_ID = res[1]

# create list of equal length to current list, basic energy level of 0.075
numTotalElements = len(index)
numInsightElements = len(insight_ESSName)

insightLink = [""] * numTotalElements

# for every element in the lattice
for i in range(0, numTotalElements):
    # search for that name in the shopping list
    insightLink[i] = "n/a"
    for j in range(0, numInsightElements):
        # if you find the name
        if essname[i] == insight_ESSName[j]:
            # the element's INSIGHT LINK is equal
            insightLink[i] = insight_ID[j]
            # print(insightLink[i], i);

# END PBI_SHOPPINGLIST_LINKS


# First we initialize parser objects
pLEBT = parseLEBT.LEBT()
pMEBT = parseMEBT.MEBT()
pDTL = parseDTL.DTL()
pSPK = parseSPK.SPK()
pMBL = parseMBL.MBL()
pHBL = parseHBL.HBL()
pHEBT = parseHEBT.HEBT()
pA2T = parseA2T.A2T()
pDmpL = parseDMPL.DMPL()

parsers = {
    "ISrc": pLEBT,
    "LEBT": pLEBT,
    "RFQ": pMEBT,
    "MEBT": pMEBT,
    "DTL": pDTL,
    "Spk": pSPK,
    "MBL": pMBL,
    "HBL": pHBL,
    "HEBT": pHEBT,
    "A2T": pA2T,
    "PBW": pA2T,
    "Mnlt": pA2T,
    "DmpL": pDmpL,
}

for (
    element,
    essname,
    insightLink,
    section,
    aperture,
    model,
    index,
    slot_number,
    slot_type,
    elementEnergy,
    TCSz,
    TCSy,
    BPSz,
    BPSy,
) in zip(
    element,
    essname,
    insightLink,
    section,
    aperture,
    model,
    index,
    slot_number,
    slot_type,
    elementEnergy,
    TCSz,
    TCSy,
    BPSz,
    BPSy,
):
    if section not in parsers:
        raise ValueError(f"Unknown section {section}")
    parsers[section].append(
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    )

# After Parsing we write all files..
pLEBT.write()
pMEBT.write()
pDTL.write()
pSPK.write()
pMBL.write()
pHBL.write()
pHEBT.write()
pA2T.write()
pDmpL.write()


# Now we create new json files,
# if we have downloaded info about previous files..
for sec in ["LEBT", "RFQ-MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "DMPL", "A2T"]:
    infofile = "info_{}.json".format(sec)
    newfile_json = os.path.join("HTML", sec, f"{sec}.json")
    newfile_html = os.path.join("HTML", sec, f"{sec}.html")

    if os.path.exists(infofile):
        import json

        # Read current page info/content
        info = json.loads(open(infofile, "r").read())

        # some information needed to generate new page:
        page_id = info["id"]
        page_title = info["title"]
        space_key = info["space"]["key"]

        version_current = info["version"]["number"]
        version = {"number": str(int(version_current) + 1)}
        if "CI_COMMIT_SHA" in os.environ:
            version["message"] = "Automatic from {} on {}@{}".format(
                os.environ["CI_COMMIT_SHA"],
                os.environ["CI_COMMIT_REF_NAME"],
                os.environ["CI_PROJECT_PATH"],
            )

        content_html = open(newfile_html, "r").read()
        storage = {"storage": {"value": content_html, "representation": "storage"}}
        data = {
            "type": "page",
            "title": page_title,
            "space": {"key": space_key},
            "version": version,
            "body": storage,
        }

        open(newfile_json, "w").write(json.dumps(data, indent=2))
