from parsebase import parsebase


class A2T(parsebase):
    def __init__(self):
        parsebase.__init__(self, "A2T")

    def append(
        self,
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    ):
        if element in (
            "QV",
            "QH",
            "BPM",
            "Grd",
            "CX",
            "Img",
            "Cav",
            "WS",
            "NPM",
            "FC",
            "BCM",
            "LBM",
            "BLM",
            "Col",
            "AptM",
            "EMU",
            "DPL",
            "DV",
            "SLIT",
            "RstH",
            "RstV",
        ):
            if model:
                base = self.dict[element][model]
            else:
                base = self.dict[element]
            if slot_type and slot_type in base:
                base = base[slot_type]

            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                model=model,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
                id=element,
            ):
                # text('index = '+index)
                self.doc.stag("img", klass="element_img", src=base["src"])

            # Special treatment of Target (does not exist in CSV)
            if element == "Img" and slot_type == "PBDPlg" and index == 1:
                with self.tag(
                    "div",
                    klass="element",
                    index=1,
                    essname="TARGET",
                    model=model,
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=(TCSz - self.dict["TGT"]["zShift"]),
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=(BPSz - self.dict["TGT"]["zShift"]),
                    id="TGT",
                ):
                    # text('index = '+index)
                    self.doc.stag("img", klass="element_img", src=self.dict["TGT"]["src"])

        elif element == "Drf":
            base = self.dict["Drf"]
            if slot_number in base and slot_type in base[slot_number] and index in base[slot_number][slot_type]:
                drf_type = base[slot_number][slot_type][index]
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname="n/a",
                    insightLink="n/a",
                    model="n/a",
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id=self.dict["Valve"][drf_type]["id"],
                ):
                    # text('index = '+index)
                    self.doc.stag(
                        "img",
                        klass="element_img",
                        src=self.dict["Valve"][drf_type]["src"],
                    )

        elif element not in ["Drf"]:
            # Print ignored elements (to check we are taking all)
            print("Ignoring element", element, "in A2T")
