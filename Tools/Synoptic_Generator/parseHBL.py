from parsebase import parsebase


class HBL(parsebase):
    def __init__(self):
        parsebase.__init__(self, "HBL")

        self.current_CM_TCS_z_start = 0
        self.current_CM_BPS_z_start = 0

    def append(
        self,
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    ):
        if element in ("QV", "QH", "CX", "BPM", "BCM", "BLM", "WS", "NPM"):
            if model:
                base = self.dict[element][model]
            else:
                base = self.dict[element]
            if slot_type and slot_type in base:
                base = base[slot_type]
            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
                id=element,
            ):
                # text('index = '+index)
                self.doc.stag("img", klass="element_img", src=base["src"])

        elif element == "Cav":
            dn100 = self.dict["Valve"]["DN100"]
            if index == 1:
                self.current_CM_TCS_z_start = TCSz
                self.current_CM_BPS_z_start = BPSz
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname="n/a",
                    insightLink="n/a",
                    model="n/a",
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id=dn100["id"],
                ):
                    # text('index = '+index)
                    self.doc.stag(
                        "img",
                        klass="element_img",
                        src=dn100["src"],
                    )

            elif index == 4:
                current_CM_TCS_z_end = TCSz
                current_CM_BPS_z_end = BPSz
                current_CM_TCS_z_middle = (self.current_CM_TCS_z_start + current_CM_TCS_z_end) / 2
                current_CM_BPS_z_middle = (self.current_CM_BPS_z_start + current_CM_BPS_z_end) / 2
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname=essname,
                    insightLink=insightLink,
                    model=model,
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=current_CM_TCS_z_middle,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=current_CM_BPS_z_middle,
                    id=element,
                ):
                    self.doc.stag("img", klass="element_img", src=self.dict[element]["src"])
                with self.tag(
                    "div",
                    klass="element",
                    index=index,
                    essname="n/a",
                    insightLink="n/a",
                    model="n/a",
                    slot_type=slot_type,
                    slot_number=slot_number,
                    aperture=aperture,
                    elementEnergy=elementEnergy,
                    section=section,
                    tcs_z=TCSz,
                    tcs_y=TCSy,
                    bps_y=BPSy,
                    bps_z=BPSz,
                    id=dn100["id"],
                ):
                    self.doc.stag(
                        "img",
                        klass="element_img",
                        src=dn100["src"],
                    )

        elif element not in ["Drf"]:
            # Print ignored elements (to check we are taking all)
            print("Ignoring element", element, "in HBL")
