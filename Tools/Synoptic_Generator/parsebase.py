import os
import sys
from yattag import indent, Doc


class parsebase:
    def __init__(self, name):
        import yaml

        self.name = name
        self.filebase = name
        dict_path = os.path.join(os.path.dirname(sys.argv[0]), "dictionary.yml")
        dict_io = open(dict_path, "r").read()
        self.dict = yaml.load(dict_io, Loader=yaml.SafeLoader)[self.name]

        self.doc, self.tag, self.text = Doc().tagtext()

    def write(self):
        macroId = "5ac28f69-aca5-4ae9-9a97-67ab4eb7f2ad"
        headerData = "<p class='auto-cursor-target'><br /></p>"
        headerData += f"<ac:structured-macro ac:name='html' ac:schema-version='1' ac:macro-id='{macroId}'><ac:plain-text-body><![CDATA["

        fin = open(os.path.join(os.path.dirname(sys.argv[0]), "htmlHeader.html"), "r")
        headerData2 = fin.read()

        confluenceFooter = "]]></ac:plain-text-body></ac:structured-macro><p class='auto-cursor-target'><br /></p>"

        combined_html = headerData + headerData2 + indent(self.doc.getvalue()) + confluenceFooter

        text_file = open(os.path.join("HTML", self.filebase, self.filebase + ".html"), "w")
        text_file.write(combined_html)
        text_file.close()
