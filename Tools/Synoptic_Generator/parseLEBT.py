from parsebase import parsebase


class LEBT(parsebase):
    def __init__(self):
        parsebase.__init__(self, "LEBT")

    def append(
        self,
        element,
        essname,
        insightLink,
        model,
        section,
        aperture,
        index,
        slot_number,
        slot_type,
        elementEnergy,
        TCSz,
        TCSy,
        BPSz,
        BPSy,
    ):
        # Add IS element
        if element == "Drf" and index == 1 and section == "ISrc":
            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                model=model,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=(TCSz - BPSz * 0.5),
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=(BPSz - BPSz * 0.5),
                id="IS",
            ):
                self.doc.stag("img", klass="element_img", src=self.dict["IS"]["src"])

        if element in (
            "FC",
            "NPM",
            "BCM",
            "Sol",
            "CX",
            "Coll",
            "Iris",
            "Chop",
            "Dpl",
            "EMU",
        ):
            if model:
                base = self.dict[element][model]
            else:
                base = self.dict[element]
            if slot_type and slot_type in base:
                base = base[slot_type]

            with self.tag(
                "div",
                klass="element",
                index=index,
                essname=essname,
                insightLink=insightLink,
                model=model,
                slot_type=slot_type,
                slot_number=slot_number,
                aperture=aperture,
                elementEnergy=elementEnergy,
                section=section,
                tcs_z=TCSz,
                tcs_y=TCSy,
                bps_y=BPSy,
                bps_z=BPSz,
                id=element,
            ):
                self.doc.stag("img", klass="element_img", src=base["src"])

        elif element not in ["Drf"]:
            # Print ignored elements (to check we are taking all)
            print("Ignoring element", element, "in", self.name)
