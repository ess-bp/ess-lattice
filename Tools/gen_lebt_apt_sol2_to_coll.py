"""
    Aperture for the LEBT solenoid 2 and collimator.

    - 50 mm for 105.0+41.0 mm drift
    - 50 mm for the initial 223.1 mm of the solenoid
    - 75 mm for the 96.9 mm of the solenoid and 29.0 mm drift.
    - 35 mm for the initial 14.062 mm of the collimator.
    - 35 mm to 7 mm over 173 mm within the collimator.
    -  7 mm for the last 2 mm of the collimator.
"""

# -- Inputs

ds = 173.0 / 200.0  # Step size within the collimator slope

# -- Libs

import numpy as np
import sys

# -- Hard-coded lengths

l_50 = 105.0 + 41.0 + 223.1
l_75 = 96.9 + 29.0
l_coll_0 = 14.062
l_coll = 173.0
l_coll_1 = 2.0

# -- Down to the collimator slope

s = np.array([0.0])
s = np.append(s, s[-1] + l_50)
apt = np.array([50.0])
apt = np.append(apt, apt[-1])

s = np.append(s, s[-1])
s = np.append(s, s[-1] + l_75)
apt = np.append(apt, 75.0)
apt = np.append(apt, apt[-1])

s = np.append(s, s[-1])
s = np.append(s, s[-1] + l_coll_0)
apt = np.append(apt, 35.0)
apt = np.append(apt, apt[-1])

# -- Collimator slope

s_coll = np.linspace(0.0, l_coll, l_coll // ds + 1)
apt_coll = 35.0 - 28.0 / 173.0 * s_coll

s_coll += s[-1]

s = np.concatenate((s, s_coll[1:]))
apt = np.concatenate((apt, apt_coll[1:]))

# -- Last 2 mm drift of the collimator

s = np.append(s, s[-1] + l_coll_1)
apt = np.append(apt, apt[-1])

# -- mm => m

s *= 1e-3
apt *= 1e-3

# -- Writing

with open("LEBT_Apt_Sol2-Coll.ouv", "w") as file:
    file.write("{:d}\n".format(len(s)))
    for i in np.arange(len(s)):
        file.write("{:.6f}  {:.5f}\n".format(s[i], apt[i]))

# --

sys.exit()

# --
