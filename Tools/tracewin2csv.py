#
# Parse Tracewin lattice data files and produce slot and element information for each section
# Tracewin data files are located in GIT: gitlab.esss.lu.se:ess-bp/ess-lattice.git
#
# Using "next" branch DAT files that have markers and DIAG_xxxx for PBI
#
# Main author: hinko.kocevar@esss.se
# Last update: 14 Aug 2018, by yngve.levinsen@esss.se
#

import sys
import re
import os
from math import fabs, ceil, sin, cos, pi
import csv


ALL_SECTIONS = ["LEBT", "RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "DMPL", "A2T"]


def open_csv(filename, mode="r"):
    """
    A trick to make opening of csv file compatible with Python 2 and 3
    """
    return open(filename, mode=mode + "b") if sys.version_info[0] == 2 else open(filename, mode=mode, newline="")


class Geo:
    def __init__(self):
        # x, y, z axis
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        # accumulated beam path length
        self.s = 0.0

    def __sub__(self, other):
        new_geo = Geo()
        new_geo.x = self.x - other.x
        new_geo.y = self.y - other.y
        new_geo.z = self.z - other.z
        new_geo.s = self.s if self.s else other.s
        return new_geo

    def __add__(self, other):
        new_geo = Geo()
        new_geo.x = self.x + other.x
        new_geo.y = self.y + other.y
        new_geo.z = self.z + other.z
        new_geo.s = self.s if self.s else other.s
        return new_geo


# Target in BPS coordinate system:
TGT = Geo()
TGT.y = 4500.0
TGT.z = 602697.154
TGT.s = 602712.835
# Dump in BPS coordinate system:
DMP = Geo()
DMP.z = 546298.165
DMP.s = DMP.z
# This is needed to reset location whenever reaching start of A2T/DmpL:
HEBT_A2T_MECH = Geo()
HEBT_A2T_MECH.z = 491800.4180600017
HEBT_A2T_MECH.s = HEBT_A2T_MECH.z


class Section:
    def __init__(self, name):
        self.fullName = name
        self.subsections = []
        # section length in path of beam
        self.length = 0.0
        # BPS geometry at the start, middle and end of section
        self.bpsStart = Geo()
        self.bpsMiddle = Geo()
        self.bpsEnd = Geo()
        # TCS geometry at the start, middle and end of section
        self.tcsStart = Geo()
        self.tcsMiddle = Geo()
        self.tcsEnd = Geo()

    def getSubsection(self, name):
        for ss in self.subsections:
            if ss.name == name:
                return ss
        return None


class Subsection:
    def __init__(self, section, cell, slot):
        self.section = section
        self.cell = cell
        self.slot = slot
        self.name = cell
        if len(slot):
            self.name += "_" + slot
        self.fullName = self.section + "_" + self.name
        self.elements = []
        # subsection length in path of beam
        self.length = 0.0
        # BPS geometry at the start, middle and end of subsection
        self.bpsStart = Geo()
        self.bpsMiddle = Geo()
        self.bpsEnd = Geo()
        # TCS geometry at the start, middle and end of subsection
        self.tcsStart = Geo()
        self.tcsMiddle = Geo()
        self.tcsEnd = Geo()


class Element:
    def __init__(self, line, orig_line=""):
        self.line = line
        self.orig_line = orig_line
        self.tokens = re.split("\s+", line)
        self.section = ""
        self.cell = ""
        self.slot = ""
        self.index = 0
        self.name = ""
        self.doorsid = ""
        self.model = ""
        self.tag = ""
        self.fullName = ""
        # element length in path of beam
        self.length = 0.0
        # element z displacement
        self.dz = 0.0
        # A survey offset only apply to TCS, BPS coord unaffected (seldomly used):
        self.survey_offset = 0.0
        # BPS geometry at the start, middle and end of element
        self.bpsStart = Geo()
        self.bpsMiddle = Geo()
        self.bpsEnd = Geo()
        # TCS geometry at the start, middle and end of element
        self.tcsStart = Geo()
        self.tcsMiddle = Geo()
        self.tcsEnd = Geo()
        # rotation of element
        self.angle = 0.0
        # rotation radius of the rotation (for BEND elements only)
        self.radius = 0.0
        # pipe aperture
        self.r = 0.0
        self.label = ""
        self.disc = ""
        self.show = False
        self.valid = False
        self.convert = True
        self.csv_ignore = False
        self.commentTokens = []
        self.dat_ignore = False
        self.from_external = False

        if not len(line.strip()) or line[:2] == "; ":
            self.valid = True
            self.convert = False
            return
        self.parseLine()

    def __repr__(self):
        return ", ".join(
            [
                f"tag {self.tag}",
                f"name parts {self.section} {self.cell}{self.slot} {self.name} {self.index}",
                f"model {self.model}",
                f"fullname {self.fullName}",
                f"length '{self.length}' mm",
                f"radius '{self.r}' mm",
                f"center BPS x,y,z '{self.bpsMiddle.x},{self.bpsMiddle.y},{self.bpsMiddle.z}' mm",
                f"center TCS x,y,z '{self.tcsMiddle.x},{self.tcsMiddle.y},{self.tcsMiddle.z}' mm",
            ]
        )

    def __getMnemonic(self):
        if len(self.commentTokens) == 0 or self.commentTokens[0].startswith("d_z_mm"):
            return ""
        return self.commentTokens[0]

    def __getDoorsID(self):
        if len(self.commentTokens) < 2 or self.commentTokens[1].startswith("d_z_mm"):
            return ""
        return self.commentTokens[1]

    def __getModel(self):
        model = self.name

        if self.name in ["BPM", "FC", "Dpl", "IBS", "AptM", "Img", "Grd"]:
            if self.doorsid in ["FBPM"]:
                model = self.doorsid
            else:
                model = self.name
        elif self.name == "BCM":
            if self.doorsid == "FBCM":
                model = "FBCM"
        elif self.name == "EMU":
            if self.doorsid == "EMU1":
                model = "Alison Scanner"
            elif self.doorsid == "EMU2":
                model = "Slit & Grid"
        elif self.name == "LBM":
            model = "BSM"
        elif self.name == "NPM":
            if self.doorsid in ["NPM1A", "NPM1B", "NPM1C", "NPM1D"]:
                model = "BIF"
            elif self.doorsid == "NPM2":
                model = "IPM"
        elif self.name == "WS":
            if self.doorsid == "WS4":
                model = "FWS"
        elif self.name == "BLM":
            if self.doorsid == "BLM1":
                model = "ICBLM"
            elif self.doorsid == "BLM2":
                model = "nBLM"
        elif self.name in ["QV", "QH"] and self.doorsid:
            model = self.doorsid
        else:
            # either not PBI disc or do not care
            model = ""

        return model

    def __getDeltaZ(self, key="d_z_mm"):
        if len(self.commentTokens) == 0:
            # print('Delta Z not found for line:', self.line)
            return 0.0
        for ct in self.commentTokens:
            if ct.startswith(key):
                return float(ct.split("=")[1])
        return 0.0

    def parseLine(self):
        # need at least three words
        if len(self.tokens) < 2:
            return
        # do we have a label?
        if self.tokens[1] == ":":
            self.label = self.tokens[0].replace("_", ":")
            self.tokens = self.tokens[2:]

        # uppercase the tracewin tag
        tag = self.tokens[0].upper()
        # if the tag has (nn) at the end, get rid of it
        self.tag = re.sub("\([0-9]*\)", "", tag)
        if self.tag.startswith(";#"):
            self.tag = self.tag[2:]

        # find structured comment enclosed in brackets: ... ; [ ... ]
        haveComments = False
        res = re.search("\[(.*)\]", self.line)
        if res and len(res.groups()) > 0:
            self.commentTokens = res.group(1).split()
            haveComments = True
        self.name = self.__getMnemonic()
        self.doorsid = self.__getDoorsID()
        self.model = self.__getModel()
        self.dz = self.__getDeltaZ()
        self.survey_offset = self.__getDeltaZ("survey_offset")

        # include the element in output by default, can be false later on
        self.show = True
        # set element short name and length
        if self.tag == "MARKER":
            self.name = self.tokens[1]
            self.show = False
        elif self.tag in ["DRIFT"]:
            self.length = float(self.tokens[1])
            self.r = float(self.tokens[2])
            # XXX: Following name is NOT in Naming Service
            self.name = "Drf"
            self.disc = ""
            # ignore drifts shorter than 1 mm
            if self.length < 1.0:
                self.show = False
        elif self.tag in ["SOLENOID"]:
            self.length = float(self.tokens[1])
            self.r = float(self.tokens[3])
            self.name = "Sol"
            self.disc = "BMD"
        elif self.tag == "RFQ_CELL":
            self.length = float(self.tokens[5])
            # XXX: Following name is NOT in Naming Service
            self.name = "RFC"
        elif self.tag == "QUAD":
            self.length = float(self.tokens[1])
            self.r = float(self.tokens[3])
            self.disc = "BMD"
            g = float(self.tokens[2])
            if g < 0.0:
                self.name = "QV"
            elif g > 0.0:
                self.name = "QH"
            else:
                print("Error: this element is not recognized as QV/QH:")
                print("Element line:", self.line)
        elif self.tag == "DTL_CEL":
            self.length = float(self.tokens[1])
            self.r = float(self.tokens[9])
            # XXX: Following name is NOT in Naming Service
            self.name = "DTC"
        elif self.tag == "FIELD_MAP":
            self.length = float(self.tokens[2])
            self.r = float(self.tokens[4])
            geom = int(self.tokens[1])
            if geom == 0:
                self.name = "Coll"
                self.disc = "PBI"
            elif geom == 50:
                self.name = "Sol"
                self.disc = "BMD"
            elif geom >= 100:
                self.name = "Cav"
                self.disc = "EMR"
            else:
                print("Error: this element is not recognized as named device:")
                print("Element line:", self.line)
        elif self.tag == "GAP":
            self.r = float(self.tokens[3])
            self.name = "Cav"
            self.disc = "EMR"
        # XXX: Not used at the moment
        #        elif self.tag == 'SUPERPOSE_MAP':
        #            self.l = float(self.tokens[1])
        #            # XXX: Following name is NOT in Naming Service
        #            self.name = 'SPM'
        #            self.show = False
        elif self.tag == "BEND":
            self.angle = fabs(float(self.tokens[1]))
            self.radius = float(self.tokens[2])
            self.length = ceil(self.angle * pi / 180.0 * self.radius)
            self.r = float(self.tokens[4])
            # XXX: Following name is NOT in Naming Service
            self.name = "DV"
            self.disc = "BMD"
        # XXX: Ignore the EDGE element
        #        elif self.tag == 'EDGE':
        #            # XXX: Following name is NOT in Naming Service
        #            self.name = 'EDG'
        #            self.r = float(self.tokens[6])
        elif self.tag in ["THIN_STEERING", "STEERER"]:
            # Assuming dual plane corrector
            if self.name == "":
                self.name = "CX"
            if self.tag == "THIN_STEERING":
                self.r = float(self.tokens[3])
            self.disc = "BMD"
            self.doorsid = ""
            self.model = ""
        elif self.tag == "DIAG_EMIT":
            if haveComments:
                self.disc = "PBI"
            else:
                # print('hiding:', self.line)
                self.show = False
        elif self.tag == "DIAG_SIZE":
            if haveComments:
                self.disc = "PBI"
            else:
                # print('hiding:', self.line)
                self.show = False
        elif self.tag == "DIAG_CURRENT":
            if haveComments:
                self.disc = "PBI"
            else:
                # print('hiding:', self.line)
                self.show = False
        elif self.tag == "DIAG_POSITION":
            if haveComments:
                self.disc = "PBI"
            if self.name == "DUAL_FUNCTION":
                # print('hiding:', self.line)
                self.show = False
        elif self.tag == "DIAG_LOSS":
            self.disc = "PBI"
        elif self.tag == "APERTURE":
            if haveComments:
                self.disc = "PBI"
            else:
                # print('hiding:', self.line)
                self.show = False
        elif self.tag in ["CHOPPER", "CHOPPER_DUMP"]:
            self.disc = "BMD"
        elif self.tag in ["COLLIMATOR"]:
            self.disc = "ID"
        elif self.tag in ["IRIS"]:
            self.disc = "ID"
        else:
            # print(' IGN :', self.tag)
            self.show = False
            # this element can be deleted..
            return False

        if "csv_ignore" in self.orig_line:
            self.show = False
            self.csv_ignore = True
        if "dat_ignore" in self.orig_line:
            self.dat_ignore = True

        self.valid = True
        # print(' OK  :', self.name, self.l, self.commentTokens)
        # element successfully parsed
        return True

    def isMarker(self):
        return self.tag == "MARKER"


def find_lattice_for_section(section):
    for f in os.listdir("."):
        if os.path.isdir(f) and len(f) > len(section) and f[-len(section) :] == section:
            return f"{f}/Beam_Physics/lattice.dat"


def print_readfile_report(filename, nelements):
    print(f"Parsed TraceWin DAT file {filename} (created {nelements} elements)..")


class Lattice:
    def __init__(self, args):
        self.folder = args.folder
        self.dat_files = [find_lattice_for_section(section) for section in args.sections]

        self.elements = []
        self.extra_files = args.extras
        self.slotIndexes = dict()
        self.elementIndexes = dict()
        self.cell = ""
        self.slot = ""
        self.section = ""
        self.subsection = ""
        self.sections = []

        self.angle = 0.0
        self.radius = 0.0
        self.outLatticeFile = open("lattice2.dat", "w")

    def run(self):
        for dat_file in self.dat_files:
            if not self.read_file(dat_file):
                raise IOError(f"Failed to read {dat_file}")

        for extra in self.extra_files:
            self.read_extra_file(extra)

        self.partition_lattice()
        self.calculate_bps()
        self.sort_elements()
        self.calculate_tcs()

        self.outputElements()
        self.outputSubsections()
        self.outputSections()
        self.outputDatFile()

        return True

    def sort_elements(self):
        all_sections = ["ISRC"] + ALL_SECTIONS + ["PBW", "MNLT"]

        def section_index(section):
            index = 99
            try:
                index = all_sections.index(section.upper())
            except ValueError:
                pass
            return index

        self.elements.sort(key=lambda x: (section_index(x.section), x.bpsMiddle.z))

    def read_extra_file(self, filename):
        with open(filename) as csv_file:
            reader = csv.DictReader(csv_file)
            nbefore = len(self.elements)
            for row in reader:
                el = Element("")
                el.valid = True
                el.convert = True
                el.dat_ignore = True
                el.csv_ignore = False
                el.show = True
                el.from_external = True

                splt = row["Name"].split(":")
                splt0 = splt[0].split("-")
                splt1 = splt[1].split("-")

                el.disc = splt1[0]
                el.name = splt1[1]
                el.model = row["Model"]
                el.label = row["Name"]
                el.section = splt0[0]
                el.cell = splt0[1][:3]
                el.slot = splt0[1][3:]
                el.index = int(splt1[2])
                el.length = float(row["Length [mm]"])
                el.bpsMiddle.x = float(row["Location:x [mm]"])
                el.bpsMiddle.y = float(row["Location:y [mm]"])
                el.bpsMiddle.z = float(row["Location:z [mm]"])
                self.elements.append(el)
            print_readfile_report(filename, len(self.elements) - nbefore)

    def outputDatFile(self):
        for el in self.elements:
            if el.dat_ignore is False:
                if el.label and ":" not in el.orig_line:
                    orig = el.orig_line
                    name = el.label.replace(":", "_")
                    cmt = ""
                    if len(orig) > 1 and orig[:2] == ";#":
                        cmt = ";# "
                        orig = orig[2:]
                    self.outLatticeFile.write(f"{cmt}{name} : {orig}" + "\n")
                else:
                    self.outLatticeFile.write(el.orig_line + "\n")

    def getSection(self, name):
        for s in self.sections:
            if s.fullName == name:
                return s
        return None

    def readLine(self, line):
        """
        Reads one line and returns the Element object
        """
        line = line.strip(" \t\n\r")

        # if csv_treat_as is put in the comment after element, use the alternative element in comment instead:
        use_line = line
        if "csv_treat_as" in line:
            use_line = line.split("csv_treat_as :")[1].lstrip()
        if len(use_line) > 2 and use_line[:2] == ";#":
            use_line = use_line[2:].lstrip()

        el = Element(use_line, line)
        if not el.valid:
            return el

        if el.name == "DV" and self.angle == 0.0:
            # this is first BEND so remember radius and angle
            self.angle = fabs(float(el.tokens[1]))
            self.radius = float(el.tokens[2])
        elif el.name == "DV" and self.angle != 0.0:
            # reset the angle and radius for after second BEND
            self.angle = 0.0
            self.radius = 0.0
        else:
            # use saved values for other elements
            el.angle = self.angle
            el.radius = self.radius

        return el

    def prependExtraElements(self, line):
        """
        Add any elements that goes before line (e.g. DTL stems)
        """

        returnList = []

        # Current hack for DTL/PMQs
        lsp = line.split()
        if lsp[0] == "DTL_CEL":
            strength = float(lsp[5])
            r = float(lsp[9])
            # returnList.append(";#DRIFT_TUBE_STEM_CENTER ; [dat_ignore]")
            if strength != 0:
                if strength > 0:
                    HV = "H"
                else:
                    HV = "V"
                halfLength = float(lsp[2])
                if halfLength < 30:  # This must be a 50mm cell
                    length = 50
                    dz = halfLength - 25
                else:  # This must be an 80mm cell
                    length = 80
                    dz = halfLength - 40
                returnList.append(f"QUAD 0.0 {strength} {r} ; [Q{HV} PMQ{HV}{length} d_z_mm={dz} dat_ignore]")

        return [self.readLine(element) for element in returnList]

    def appendExtraElements(self, line):
        """
        Add any elements that goes after line (placeholder for now)
        """

        returnList = []

        return [self.readLine(element) for element in returnList]

    def writeLine(self, element=None, line=""):
        if not element and not line:
            raise ValueError("Need element OR line to be defined")
        elif element and line:
            raise ValueError("Need element OR line to be defined")
        if line:
            self.outLatticeFile.write(line)

    def read_file(self, dat_file):
        nbefore = len(self.elements)
        with open(dat_file, "r") as f:
            for line in f:
                if len(line.strip()):
                    for el in self.prependExtraElements(line):
                        self.elements.append(el)
                el = self.readLine(line)
                if not el.valid:
                    el.show = False
                self.elements.append(el)
                for el in self.appendExtraElements(line):
                    self.elements.append(el)
        print_readfile_report(dat_file, len(self.elements) - nbefore)
        return True

    def partition_lattice(self):
        for el in self.elements:
            if el.csv_ignore or el.from_external or not (el.convert & el.valid):
                continue
            if el.isMarker():
                self.handle_marker(el)
            else:
                # element indexes
                if el.name in self.elementIndexes:
                    self.elementIndexes[el.name] += 1
                else:
                    self.elementIndexes[el.name] = 1

                # basic params
                el.section = self.section
                el.cell = self.cell
                el.slot = self.slot
                el.index = self.elementIndexes[el.name]
                if len(el.slot):
                    el.fullName = el.section + "_" + el.cell + "_" + el.slot + "_" + el.name + "_" + "%03d" % el.index
                else:
                    el.fullName = el.section + "_" + el.cell + "_" + el.name + "_" + "%03d" % el.index
                # add to the subsection
                section = self.getSection(self.section)
                if section is not None:
                    subsection = section.getSubsection(self.subsection)
                    subsection.elements.append(el)

        # get rid of the empty subsections
        for s in self.sections:
            for ss in s.subsections:
                if len(ss.elements) == 0:
                    s.subsections.remove(ss)

    def calculate_bps(self):
        # calculate BPS for elements
        degToRad = pi / 180.0
        # current lattice geometry
        x = 0.0
        y = 0.0
        z = 0.0
        s = 0.0
        r = 0.0

        for el in self.elements:
            if el.isMarker():
                if el.name.upper() in ["HEBT_A2T_MECHANICAL", "HEBT_DMPL_OPTICAL"]:
                    if sys.flags.debug:
                        print("Reset coordinates for HEB/A2T mech interface")
                    # Reset coordinates to HEBT-A2T interface
                    x = HEBT_A2T_MECH.x
                    y = HEBT_A2T_MECH.y
                    z = HEBT_A2T_MECH.z
                    s = HEBT_A2T_MECH.s
                    r = 50.0
            if el.bpsMiddle.z != 0:
                # This element already has calculated position, ie from external CSV tables
                # TODO consider orientation/rotation + length of elements
                el.bpsStart.x = el.bpsMiddle.x
                el.bpsStart.y = el.bpsMiddle.y
                el.bpsStart.z = el.bpsMiddle.z - 0.5 * el.length
                el.bpsEnd.x = el.bpsMiddle.x
                el.bpsEnd.y = el.bpsMiddle.y
                el.bpsEnd.z = el.bpsMiddle.z + 0.5 * el.length
                continue

            if el.csv_ignore or not (el.valid & el.convert):
                continue
            # dy is increase up, in Y direction
            # dz is increase along the lattice, not beam path
            if el.name == "DV":
                dz = el.radius * sin(el.angle * degToRad)
                dy = 2 * el.radius * sin(el.angle / 2.0 * degToRad) * sin(el.angle / 2.0 * degToRad)
            else:
                dz = el.length * cos(el.angle * degToRad)
                dy = el.length * sin(el.angle * degToRad)

            el.bpsStart.x = x
            el.bpsStart.y = y
            el.bpsStart.z = z + el.dz
            el.bpsStart.s = s + el.dz
            el.bpsMiddle.x = x
            el.bpsMiddle.y = y + dy / 2.0
            el.bpsMiddle.z = z + el.dz + dz / 2.0
            el.bpsMiddle.s = s + el.dz + el.length / 2.0
            el.bpsEnd.x = x
            el.bpsEnd.y = y + dy
            el.bpsEnd.z = z + el.dz + dz
            el.bpsEnd.s = s + el.dz + el.length

            if el.length > 0.0:
                x = el.bpsEnd.x
                y = el.bpsEnd.y
                z = el.bpsEnd.z
                s = el.bpsEnd.s

            if el.r > 0.0:
                r = el.r

            # set element aperture if needed
            if el.r == 0.0:
                el.r = r

        # calculate BPS for sections and subsections
        for s in self.sections:
            # first calculate subsections positions
            for ss in s.subsections:
                # start is at first element of subsection
                ss.bpsStart.x = ss.elements[0].bpsStart.x
                ss.bpsStart.y = ss.elements[0].bpsStart.y
                ss.bpsStart.z = ss.elements[0].bpsStart.z
                ss.bpsStart.s = ss.elements[0].bpsStart.s
                # end is at the last element of subsection
                ss.bpsEnd.x = ss.elements[-1].bpsEnd.x
                ss.bpsEnd.y = ss.elements[-1].bpsEnd.y
                ss.bpsEnd.z = ss.elements[-1].bpsEnd.z
                ss.bpsEnd.s = ss.elements[-1].bpsEnd.s
                # middle is the middle point between end and start
                ss.bpsMiddle.x = (ss.bpsEnd.x - ss.bpsStart.x) / 2.0 + ss.bpsStart.x
                ss.bpsMiddle.y = (ss.bpsEnd.y - ss.bpsStart.y) / 2.0 + ss.bpsStart.y
                ss.bpsMiddle.z = (ss.bpsEnd.z - ss.bpsStart.z) / 2.0 + ss.bpsStart.z
                ss.bpsMiddle.s = (ss.bpsEnd.s - ss.bpsStart.s) / 2.0 + ss.bpsStart.s
                # length of the subsection
                ss.length = ss.bpsEnd.z - ss.bpsStart.z

            # second calculate the section position using subsection position calculated above
            # start is at first aubsection of section
            s.bpsStart.x = s.subsections[0].bpsStart.x
            s.bpsStart.y = s.subsections[0].bpsStart.y
            s.bpsStart.z = s.subsections[0].bpsStart.z
            s.bpsStart.s = s.subsections[0].bpsStart.s
            # end is at last aubsection of section
            s.bpsEnd.x = s.subsections[-1].bpsEnd.x
            s.bpsEnd.y = s.subsections[-1].bpsEnd.y
            s.bpsEnd.z = s.subsections[-1].bpsEnd.z
            s.bpsEnd.s = s.subsections[-1].bpsEnd.s
            # middle is the middle point between end and start
            s.bpsMiddle.x = (s.bpsEnd.x - s.bpsStart.x) / 2.0 + s.bpsStart.x
            s.bpsMiddle.y = (s.bpsEnd.y - s.bpsStart.y) / 2.0 + s.bpsStart.y
            s.bpsMiddle.z = (s.bpsEnd.z - s.bpsStart.z) / 2.0 + s.bpsStart.z
            s.bpsMiddle.s = (s.bpsEnd.s - s.bpsStart.s) / 2.0 + s.bpsStart.s
            # length of the section
            s.length = s.bpsEnd.z - s.bpsStart.z

    def calculate_tcs(self):
        # calculate TCS/DCS for elements
        for el in self.elements:
            if el.csv_ignore or not (el.valid & el.convert):
                continue
            el.tcsStart = TGT - el.bpsStart
            el.tcsStart.z -= el.survey_offset  # Survey offsets only apply to TCS
            el.tcsMiddle = el.tcsStart - el.bpsMiddle + el.bpsStart
            el.tcsEnd = el.tcsStart - el.bpsEnd + el.bpsStart

        # calculate TCS/DCS for sections and subsections
        for s in self.sections:
            for ss in s.subsections:
                ss.tcsStart = TGT - ss.bpsStart
                ss.tcsMiddle = TGT - ss.bpsMiddle
                ss.tcsEnd = TGT - ss.bpsEnd

            # second calculate the section position using subsection position calculated above
            s.tcsStart = TGT - s.bpsStart
            s.tcsMiddle = TGT - s.bpsMiddle
            s.tcsEnd = TGT - s.bpsEnd

    def handle_marker(self, element):
        slotIndex = 1
        slotIndexInc = True

        # uppercase to make the parsing more roboust to char case
        name = element.name.upper()

        if re.match("ISRC_LEBT_OPTICAL", name):
            self.section = "ISrc"
            self.slot = ""
            self.slotIndexes = dict()
        elif re.match("ISRC_LEBT_MECHANICAL", name):
            self.section = "LEBT"
            self.slot = ""
            self.slotIndexes = dict()
        elif re.match("LEBT_RFQ_OPTICAL", name):
            self.section = "RFQ"
            self.slot = ""
            self.slotIndexes = dict()
        elif re.match("RFQ_MEBT_MECHANICAL", name):
            self.section = "MEBT"
            self.slot = ""
            self.slotIndexes = dict()
        elif re.match("MEBT_DTL_MECHANICAL", name):
            self.section = "DTL"
            self.slot = ""
            self.slotIndexes = dict()
        elif re.match("DTL_\d_TO_\d_INTERTANK", name):
            self.slot = ""
        elif re.match("DTL_SPOKE_MECHANICAL", name):
            self.section = "Spk"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("CRYO_SPK_IN_\d\d", name):
            self.slot = "Crm"
            self.elementIndexes = dict()
            slotIndexInc = False
        elif re.match("LWU_SPK_IN_\d\d", name):
            self.slot = "LWU"
            self.elementIndexes = dict()
        elif re.match("SPK_MBL_MECHANICAL", name):
            self.section = "MBL"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("CRYO_MBL_IN_\d\d", name):
            self.slot = "Crm"
            self.elementIndexes = dict()
            slotIndexInc = False
        elif re.match("LWU_MBL_IN_\d\d", name):
            self.slot = "LWU"
            self.elementIndexes = dict()
        elif re.match("MBL_HBL_MECHANICAL", name):
            self.section = "HBL"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("CRYO_HBL_IN_\d\d", name):
            self.slot = "Crm"
            self.elementIndexes = dict()
            slotIndexInc = False
        elif re.match("LWU_HBL_IN_\d\d", name):
            self.slot = "LWU"
            self.elementIndexes = dict()
        elif re.match("HBL_HEBT_MECHANICAL", name):
            self.section = "HEBT"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("DRIFT_HEBT_IN_\d\d", name):
            self.slot = "Drf"
            self.elementIndexes = dict()
            slotIndexInc = False
        elif re.match("LWU_HEBT_IN_\d\d", name):
            self.slot = "LWU"
            self.elementIndexes = dict()
        elif re.match("DIPOLE_01_IN_17", name):
            # use last LWU + 1 index for dipole 1
            slotIndex = self.slotIndexes["LWU"] + 1
            self.slot = "DV"
        elif re.match("DIPOLE_01_OUT_17", name):
            slotIndexInc = False
        elif re.match("HEBT_A2T_MECHANICAL", name):
            # Reset in case DMPL was parsed first
            self.slotIndexes["LWU"] = 16
            self.section = "HEBT"
        elif re.match("HEBT_A2T_OPTICAL", name):
            self.section = "A2T"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("LWU_DOG_IN_\d\d", name):
            self.slot = "LWU"
        elif re.match("DRIFT_DOG_IN_\d\d", name):
            self.slot = "Drf"
        elif re.match("DIPOLE_02_IN_07", name):
            # use last drift index for dipole 2
            slotIndex = self.slotIndexes[self.slot]
            self.slot = "DV"
        elif re.match("DIPOLE_02_OUT_07", name):
            slotIndexInc = False
            self.slotIndexes["LWU"] = self.slotIndexes[self.slot]
        elif re.match("LWU_A2T_IN_\d\d", name):
            self.slot = "LWU"
        elif re.match("DRIFT_A2T_IN_\d\d", name):
            self.slot = "Drf"
        elif re.match("RASTER_IN_01", name):
            slotIndex = self.slotIndexes[self.slot]
            self.slot = "RSM"
        elif re.match("RASTER_IN_02", name):
            slotIndex = self.slotIndexes[self.slot]
            self.slot = "RSM"
            self.slotIndexes["LWU"] += 2
        elif re.match("NEUTRON_BACKSHINE_SHIELD__UPSTREAM_EDGE", name):
            slotIndex = self.slotIndexes[self.slot]
            self.slot = "NSW"
        elif re.match("NEUTRON_BACKSHINE_SHIELD__DOWNSTREAM_EDGE", name):
            slotIndex = self.slotIndexes[self.slot] + 1
            self.slot = "D2T"
        elif re.match("PROTON_BEAM_WINDOW", name):
            self.section = "PBW"
            self.slot = "Plg"
            self.cell = ""
            slotIndexInc = False
            self.elementIndexes = dict()
        elif re.match("PBI_PLUG_CENTER", name):
            self.section = "Mnlt"
            self.slot = "PBDPlg"
            self.cell = ""
            slotIndexInc = False
            self.elementIndexes = dict()
        elif re.match("HEBT_DMPL_OPTICAL", name):
            self.section = "DmpL"
            self.slot = None
            self.slotIndexes = dict()
        elif re.match("QC_DMPL_IN_\d\d", name):
            self.slot = "QC"
            self.elementIndexes = dict()
            slotIndexInc = False
        #        elif re.match('DMPL_QC_Out_\d\d', name):
        #            self.slot = 'DRF'
        elif re.match("DRIFT_DMPL_IN_\d\d", name):
            self.slot = "Drf"
            self.elementIndexes = dict()
        else:
            return False

        if self.slot is None:
            return True

        if slotIndexInc:
            if self.slot in self.slotIndexes:
                self.slotIndexes[self.slot] += 1
            else:
                self.slotIndexes[self.slot] = slotIndex
            self.cell = "%03d" % (self.slotIndexes[self.slot] * 10)
            self.elementIndexes = dict()

        s = self.getSection(self.section)
        if not s:
            s = Section(self.section)
            self.sections.append(s)

        self.subsection = self.cell
        if len(self.slot):
            self.subsection += "_" + self.slot
        ss = s.getSubsection(self.subsection)
        if not ss:
            s.subsections.append(Subsection(self.section, self.cell, self.slot))

        return True

    def outputElements(self):
        # print('********************** ELEMENTS GEOMETRY **********************')
        import csv

        filename = self.folder + "/elements.csv"
        with open_csv(filename, "w") as filehandle:
            csvwriter = csv.writer(filehandle, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            hdr = ["sect", "cell", "slot", "disc", "elem", "index", "model", "essname"]
            hdr += ["len [mm]", "r [mm]"]
            hdr += ["s:s [mm]", "BPS:s:x [mm]", "BPS:s:y [mm]", "BPS:s:z [mm]"]
            hdr += ["s:m [mm]", "BPS:m:x [mm]", "BPS:m:y [mm]", "BPS:m:z [mm]"]
            hdr += ["s:e [mm]", "BPS:e:x [mm]", "BPS:e:y [mm]", "BPS:e:z [mm]"]
            hdr += ["TCS:s:x [mm]", "TCS:s:y [mm]", "TCS:s:z [mm]"]
            hdr += ["TCS:m:x [mm]", "TCS:m:y [mm]", "TCS:m:z [mm]"]
            hdr += ["TCS:e:x [mm]", "TCS:e:y [mm]", "TCS:e:z [mm]"]
            # print(hdr)
            csvwriter.writerow(hdr)
            cnt = 0

            # output BPS
            i = 0
            self.elements[i]
            while self.elements[i].csv_ignore or not (self.elements[i].valid & self.elements[i].convert):
                i += 1
            element = self.elements[i]
            d = ["", "", "", "", "", "", "", "BPS", 0.0, 0.0]
            for cs in [element.bpsStart] * 3:
                d += [cs.s, cs.x, cs.y, cs.z]
            for cs in [element.tcsStart] * 3:
                d += [cs.x, cs.y, cs.z]
            # fix float formats:
            d[9] = float("%.3f" % d[9])
            for i in range(11, len(d)):
                d[i] = float("%.3f" % d[i])

            # print(d)
            csvwriter.writerow(d)
            cnt += 1

            prevElement = None
            for element in self.elements:
                if not element.show:
                    continue

                # Take some data from previous elements when not available
                # Particularly relevant for external elements
                if element.length == 0.0 and prevElement:
                    if element.bpsStart.s == 0.0:
                        element.bpsStart.s = prevElement.bpsEnd.s
                        element.bpsMiddle.s = prevElement.bpsEnd.s
                        element.bpsEnd.s = prevElement.bpsEnd.s
                    if element.r == 0.0:
                        element.r = prevElement.r

                if not element.label and element.name not in ["Drf", "RFC", "DTC"]:
                    element.label = "%s-%s%s:%s-%s-%03d" % (
                        element.section,
                        element.cell,
                        element.slot,
                        element.disc,
                        element.name,
                        element.index,
                    )

                d = [
                    element.section,
                    element.cell,
                    element.slot,
                    element.disc,
                    element.name,
                    "%03d" % element.index,
                    element.model,
                    element.label,
                    element.length,
                    element.r,
                ]
                for cs in [element.bpsStart, element.bpsMiddle, element.bpsEnd]:
                    d += [cs.s, cs.x, cs.y, cs.z]
                for cs in [element.tcsStart, element.tcsMiddle, element.tcsEnd]:
                    d += [cs.x, cs.y, cs.z]

                # fix float formats:
                d[9] = float("%.3f" % d[9])
                for i in range(11, len(d)):
                    d[i] = float("%.3f" % d[i])

                # print(d)
                csvwriter.writerow(d)
                cnt += 1
                prevElement = element

            i = -1
            self.elements[i]
            while self.elements[i].csv_ignore or not (self.elements[i].valid & self.elements[i].convert):
                i -= 1
            element = self.elements[i]
            for elem, element in zip(["TCS", "DCS"], [TGT, DMP]):
                d = ["", "", "", "", "", "", "", elem, 0.0, 0.0]
                for cs in [element] * 3:
                    d += [cs.s, cs.x, cs.y, cs.z]
                for cs in [TGT - element] * 3:
                    d += [cs.x, cs.y, cs.z]

                # fix float formats:
                d[9] = float("%.3f" % d[9])
                for i in range(11, len(d)):
                    d[i] = float("%.3f" % d[i])

                # print(d)
                csvwriter.writerow(d)
                cnt += 1

        print("Created %s CSV file (saved %d elements).." % (filename, cnt))
        return True

    def outputSubsections(self):
        # print('********************** SUBSECTIONS GEOMETRY **********************')
        import csv

        filename = self.folder + "/subsections.csv"
        with open_csv(filename, "w") as filehandle:
            csvwriter = csv.writer(filehandle, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            hdr = ["sect", "cell", "slot", "essname"]
            hdr += ["len [mm]"]
            hdr += ["s:s [mm]", "BPS:s:x [mm]", "BPS:s:y [mm]", "BPS:s:z [mm]"]
            hdr += ["s:m [mm]", "BPS:m:x [mm]", "BPS:m:y [mm]", "BPS:m:z [mm]"]
            hdr += ["s:e [mm]", "BPS:e:x [mm]", "BPS:e:y [mm]", "BPS:e:z [mm]"]
            hdr += ["TCS:s:x [mm]", "TCS:s:y [mm]", "TCS:s:z [mm]"]
            hdr += ["TCS:m:x [mm]", "TCS:m:y [mm]", "TCS:m:z [mm]"]
            hdr += ["TCS:e:x [mm]", "TCS:e:y [mm]", "TCS:e:z [mm]"]
            # print(hdr)
            csvwriter.writerow(hdr)

            cnt = 0
            for s in self.sections:
                for ss in s.subsections:
                    essname = "%s-%s%s" % (ss.section, ss.cell, ss.slot)
                    d = [ss.section, ss.cell, ss.slot, essname, ss.length]
                    for cs in [ss.bpsStart, ss.bpsMiddle, ss.bpsEnd]:
                        d += [cs.s, cs.x, cs.y, cs.z]
                    for cs in [ss.tcsStart, ss.tcsMiddle, ss.tcsEnd]:
                        d += [cs.x, cs.y, cs.z]

                    # fix float formats:
                    for i in range(4, len(d)):
                        d[i] = float("%.3f" % d[i])

                    # print(d)
                    csvwriter.writerow(d)
                    cnt += 1

        print("Created %s CSV file (saved %d subsections).." % (filename, cnt))
        return True

    def outputSections(self):
        # print('********************** SECTIONS GEOMETRY **********************')
        import csv

        filename = self.folder + "/sections.csv"
        with open_csv(filename, "w") as filehandle:
            csvwriter = csv.writer(filehandle, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            hdr = ["sect"]
            hdr += ["len [mm]", "s [mm]"]
            hdr += ["BPS:s:x [mm]", "BPS:s:y [mm]", "BPS:s:z [mm]"]
            hdr += ["BPS:m:x [mm]", "BPS:m:y [mm]", "BPS:m:z [mm]"]
            hdr += ["BPS:e:x [mm]", "BPS:e:y [mm]", "BPS:e:z [mm]"]
            hdr += ["TCS:s:x [mm]", "TCS:s:y [mm]", "TCS:s:z [mm]"]
            hdr += ["TCS:m:x [mm]", "TCS:m:y [mm]", "TCS:m:z [mm]"]
            hdr += ["TCS:e:x [mm]", "TCS:e:y [mm]", "TCS:e:z [mm]"]
            csvwriter.writerow(hdr)
            cnt = 0
            for s in self.sections:
                d = [s.fullName, s.length, s.bpsStart.s]
                for cs in [
                    s.bpsStart,
                    s.bpsMiddle,
                    s.bpsEnd,
                    s.tcsStart,
                    s.tcsMiddle,
                    s.tcsEnd,
                ]:
                    d += [cs.x, cs.y, cs.z]

                # fix float formats:
                for i in range(1, len(d)):
                    d[i] = float("%.3f" % d[i])

                csvwriter.writerow(d)
                cnt += 1

        print("Created %s CSV file (saved %d sections).." % (filename, cnt))
        return True


def check_extras(extras):
    for path in extras:
        if not os.path.isfile(path):
            raise ValueError(f"{path} does not exist")
        if path.split(".")[-1] != "csv":
            raise ValueError(f"{path} should be a .csv file")
        with open(path) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for key in [
                    "Name",
                    "Model",
                    "Length [mm]",
                    "Location:x [mm]",
                    "Location:y [mm]",
                    "Location:z [mm]",
                    "Orientation:x [deg]",
                    "Orientation:y [deg]",
                ]:
                    if key not in row:
                        raise ValueError(f"{path} does not contain column {key}")
                break


def run(args):
    if args.extras:
        check_extras(args.extras)
    lattice = Lattice(args)
    ret = lattice.run()
    if not ret:
        print("Failed!")
        return 1

    print("Success!")
    return 0


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Convert TraceWin lattice to CSV..")
    parser.add_argument("-f", dest="folder", default="CSV_Tables")
    parser.add_argument("-s", dest="sections", default=ALL_SECTIONS)
    parser.add_argument("-e", dest="extras", help="Extra data in CSV tables", nargs="+", default=[])
    args = parser.parse_args()

    ret = run(args)
    exit(ret)
