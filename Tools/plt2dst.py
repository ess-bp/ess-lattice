from ess import TraceWin


def get_interface_str(position):
    l_rfq = 0.0  # the rfq is at start of our sim
    l_mebt = 4.0
    l_dtl = 39.0
    l_spk = 55.9
    l_mbl = 76.7
    l_hbl = 178.9
    l_hebt = 130.1

    lengths = [l_rfq, l_mebt, l_dtl, l_spk, l_mbl, l_hbl, l_hebt]
    names = ["RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT"]
    margins = 0.2
    l_tot = 0.0

    for length, name in zip(lengths, names):
        l_tot += length
        if l_tot - margins < position and position < l_tot + margins:
            print(position, name)
            return name

    raise ValueError("Position {} is not at interface".format(position))


data = TraceWin.plt("calc_mebt/dtl1.plt")
for i in data.Nelp:
    data.save_dst(i, "part_{}_output.dst".format(get_interface_str(data[i]["Zgen"] / 100)))
