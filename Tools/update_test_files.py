from __future__ import print_function
import os
import sys
import shutil
import subprocess
import glob

nums = ["1.0", "2.0", "3.0", "4.0", "5.0", "6.0", "7.0", "8.0", "9.0", "9.1"]
names = ["LEBT", "RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T", "DMPL"]


def select_binary():
    import platform

    if platform.system() == "Darwin":
        binary = "tracemac64"
    elif platform.system() == "Linux":
        binary = "tracelx64"
    fullpath = os.path.join(os.path.expanduser("~"), "TraceWin", "exe", binary)
    print("Using", fullpath)
    if not os.path.isfile(fullpath):
        raise ValueError("Cannot find binary, please select 'Exe - Extract Console Executables' in TW menu")
    if not os.path.isfile(binary):
        if os.path.islink(binary):
            # faulty symlink
            os.remove(binary)
        subprocess.call(f"ln -s {fullpath} .", shell=True)
    return "./" + binary


for path in ["part_dtl1.dst", "part_prev.dst", "*_new.ini"]:
    for f in glob.glob(path):
        os.remove(f)

UPDATE_INPUT_DIST = False

if len(sys.argv) > 1 and "CSV" in sys.argv:
    print("Updating CSV Tables")

    extra_files_list = ["CSV_Tables/PBI/BLM.csv"]
    extra_files = " ".join(extra_files_list)
    subprocess.call(f"python Tools/tracewin2csv.py -e {extra_files}", shell=True)

    print("Updating MD Tables")
    subprocess.call("./Tools/create_mdtables.sh", shell=True)

for num, name in zip(nums, names):
    if len(sys.argv) > 1 and name not in sys.argv:
        for path in ["part_dtl1.dst", "part_prev.dst", "*_new.ini"]:
            for f in glob.glob(path):
                os.remove(f)
        continue
    if name == "DMPL":
        # to make sure we do not make a mess with input file
        for path in ["part_dtl1.dst", "part_prev.dst", "*_new.ini"]:
            for f in glob.glob(path):
                os.remove(f)
    print("Name:", name)
    if name == "DMPL":
        projFile = "A2T.ini"
    else:
        projFile = f"{name}.ini"

    if UPDATE_INPUT_DIST or name in ["RFQ"]:
        cmd = [select_binary(), projFile, "hide_esc", "nbr_part1=100000", "partran=1"]
        if name in ["RFQ"]:
            cmd.append("toutatis=1")
        if os.path.isfile("part_prev.dst"):
            cmd.append("dst_file1=part_prev.dst")
    else:
        cmd = [select_binary(), projFile, "hide_esc", "partran=0", "toutatis=0"]

    shutil.copy(f"ProjectFiles/{projFile}", projFile)
    subprocess.call(f"python Tools/merger.py -o {name} -b", shell=True)
    print(" ".join(cmd))
    retval = subprocess.call(cmd)
    if retval:
        raise ValueError(f"TraceWin returned error {retval} when simulationg the {name}")

    if UPDATE_INPUT_DIST:
        shutil.copy("part_dtl1.dst", "part_prev.dst")
        shutil.move("part_dtl1.dst", f"part_{name}.dst")
    folder = f"{num}_{name}"
    if name not in ["RFQ"]:
        tablefile = "tracewin.out"
    else:
        tablefile = "partran1.out"
    shutil.copy(tablefile, f"{folder}/Beam_Physics/OutputFiles/{tablefile}")
    if name != "DMPL" and UPDATE_INPUT_DIST:
        shutil.copy(f"{name}_new.ini", f"ProjectFiles/{projFile}")

# Set the major lattice file back to original..
subprocess.call("python Tools/merger.py", shell=True)
