import yaml
import requests
import argparse
import subprocess
import os

"""
This script is a simple way to manually run the commands normally run by the Gitlab-CI script

Meant primarily for testing purposes (so you can check the page before commit)

Only in very rare cases should this be used against baseline
"""


def run_curl(command):
    out_file = ""
    in_file = ""
    action = ""
    user = ""
    pwd = ""
    url = ""

    is_user_pwd = False
    is_action = False
    is_outfile = False
    for sub in command.split()[1:]:
        if is_user_pwd:
            user_pwd = sub.replace('"', "")
            user, pwd = user_pwd.split(":")
        elif is_action:
            action = sub
        elif is_outfile:
            out_file = sub
        elif sub[:5] == "https":
            url = sub
        elif sub[:3] == "-d@":
            in_file = sub[3:]

        is_user_pwd = False
        is_action = False
        is_outfile = False
        if sub == "-u":
            is_user_pwd = True
        elif sub == "-X":
            is_action = True
        elif sub == "-o":
            is_outfile = True

    if not user:
        # Should not really be hardcoded but ok..
        auth = open("password.txt", "r").read().strip()
        for i, asp in enumerate(auth.split()):
            if asp == "login":
                user = auth.split()[i + 1]
            elif asp == "password":
                pwd = auth.split()[i + 1]

    if action == "GET":
        with open(out_file, "w") as f:
            r = requests.get(url, auth=(user, pwd))
            f.write(r.text)
        r.raise_for_status()
    elif action == "PUT":
        r = requests.put(
            url,
            data=open(in_file, "r").read(),
            auth=(user, pwd),
            headers={"Content-Type": "application/json", "Accept": "application/json"},
        )
        r.raise_for_status()
    else:
        raise ValueError("Unknown action {}".format(action))


def main():
    fin = open(".gitlab-ci.yml", "r")
    data = yaml.load(fin, Loader=yaml.SafeLoader)

    parser = argparse.ArgumentParser(description="Run manually the upload script normally done by GitLab-CI")
    parser.add_argument("branch", choices=["baseline", "next", "next-thomas", "next-yngve"])
    parser.add_argument("--run", "-r", action="store_true", help="Run commands (default just prints to stdout)")
    parser.add_argument(
        "--exclude",
        choices=["LEBT", "RFQ-MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T", "DMPL"],
        nargs="+",
        help="Exclude given sections",
    )
    parser.add_argument(
        "--only", choices=["LEBT", "RFQ-MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T", "DMPL"], nargs="+", help="Only given sections"
    )
    args = parser.parse_args()

    variables = data["variables"]
    variables.update(data["synoptic-" + args.branch]["variables"])

    for cmd in data["synoptic-" + args.branch]["script"]:
        if cmd.split()[0] == "curl":
            exclude = False
            # deal with only sections:
            if args.only:
                exclude = True
                for o in args.only:
                    if o in cmd:
                        exclude = False
            # deal with excluded sections:
            if args.exclude:
                for e in args.exclude:
                    if e in cmd:
                        exclude = True
            if exclude:
                continue
        if not os.path.isfile("password.txt"):
            open("password.txt", "w").write("machine confluence.esss.lu.se login <USERNAME> password <PASSWORD>")
            raise ValueError("Please insert your username and password in password.txt")
        cmd = cmd.replace('-u "${WIKI_USER}":"${WIKI_PASS}"', "--netrc-file password.txt")
        cmd = cmd.replace("$", "")

        print(cmd.format(**variables))
        if args.run:
            if cmd.split()[0] == "curl":
                run_curl(cmd.format(**variables))
            else:
                subprocess.check_call(cmd.format(**variables), shell=True)


if __name__ == "__main__":
    main()
