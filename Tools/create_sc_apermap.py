import numpy as np


# All distances in mm unless otherwise specified


# --   HBL   --
# Output file name
out = "Field_Maps/1D/HB_W_coupler_new.ouv"
# Total field map length
l_fmap = 1500
# The diameter of the initial beam pipe
d0 = 139.8
# length of initial/end of pipe (assume symmetry)
l0 = 200.0
# length of main beam pipe
l1 = 916.3
# The minimal aperture within the cells (ignoring initial/end of pipe)
dmin = 119.77
# The maximal aperture
dmax = 382.54
# main radii
r1 = 66.97
# radius of minimum curvature
r2 = 16.96
# number of cells
ncells = 5
# number of points per half-cell
np0 = 20
# -- END HBL --

# --   MBL   --
# -- END MBL --

print(l_fmap, l0)
if l_fmap < l1:
    raise ValueError("Field map length must be longer than the length of the cells")
else:
    # l0 should be corrected wrt field map length, not length of physical cavity
    l0 = (l_fmap - l1) / 2.0
# The initial drift before first cell (and after last)

z0 = np.array([0, l0])
r0 = np.array([d0 / 2.0, d0 / 2.0])

# Create x array
z_short = np.arange(0, l1 / ncells / 2, l1 / np0 / ncells / 2)
r_short = np.zeros(len(z_short))

# Create one quadrant
for i in range(len(z_short)):
    if z_short[i] < r2:
        # on the lower end of small circle
        r_short[i] = dmin / 2 + r2 - np.sqrt(r2**2 - z_short[i] ** 2)
    elif z_short[i] > l1 / 10 - r1:
        # on the higher end of big circle
        r_short[i] = dmax / 2 - r1 + np.sqrt(r1**2 - (l1 / 10 - z_short[i]) ** 2)
    else:
        # between, do simple interpolation
        xd = l1 / 10 - r1 - r2
        y1 = dmin / 2 + r2
        y2 = dmax / 2 - r1
        r_short[i] = y1 + (y2 - y1) / xd * (z_short[i] - r2)


# Copy quadrant 10 times (mirror every 2nd)

z1 = z_short.copy()
r3 = r_short.copy()
for i in range(1, ncells * 2):
    z1 = np.append(z1, z_short + l1 / ncells / 2 * i)
    if i % 2 == 0:
        r3 = np.append(r3, r_short)
    else:
        r3 = np.append(r3, r_short[::-1])

# Cut off beginning and end until not below initial beam pipe..
# again, this is not quite accurate
while r3[0] < d0 / 2:
    r3 = np.delete(r3, 0)
    z1 = np.delete(z1, 0)
while r3[-1] < d0 / 2:
    r3 = np.delete(r3, -1)
    z1 = np.delete(z1, -1)

# Add beginning and end..

z = np.append(z0, z1 + l0)
r = np.append(r0, r3)
z = np.append(z, z0 + l0 + l1)
r = np.append(r, r0)


# Save to file..
with open(out, "w") as output:
    output.write("{}\n".format(len(r)))
    for i in range(len(r)):
        output.write("{} {}\n".format(z[i] / 1000, r[i] / 1000))


#  Analysis..
analyse = False
if analyse:
    from matplotlib import pyplot as plt

    f1 = np.loadtxt(out, skiprows=1)
    f2 = np.loadtxt("Field_Maps/1D/HB_W_coupler.ouv", skiprows=1)
    f3 = np.loadtxt("Field_Maps/3D/HB_W_coupler.ouv", skiprows=1)
    plt.plot(f2[:, 0], f2[:, 1], "b-", label="orig")
    plt.plot(f3[:, 0], f3[:, 1], "g-", label="orig, 3D")
    plt.plot(f1[:, 0], f1[:, 1], "r-", label="new")
    plt.legend()
    print(f1.shape)
    plt.show()
