import os
import subprocess
from ess import TraceWin
from matplotlib import pyplot as plt

base = "public"


def makedir(path):
    if not os.path.exists(path):
        os.makedirs(path)


if "CI_COMMIT_BRANCH" in os.environ:
    git_branch = os.environ["CI_COMMIT_BRANCH"]
else:
    git_branch = subprocess.check_output(["git", "rev-parse", "--abbrev-ref", "HEAD"]).strip().decode("utf-8")
if "CI_COMMIT_SHORT_SHA" in os.environ:
    git_hash = os.environ["CI_COMMIT_SHORT_SHA"]
else:
    git_hash = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"]).strip().decode("utf-8")
if "CI_COMMIT_TAG" in os.environ:
    git_tag = os.environ["CI_COMMIT_TAG"]
else:
    try:
        git_tag = subprocess.check_output(["git", "describe", "--exact-match", "--tags", git_hash]).strip().decode("utf-8")
    except subprocess.CalledProcessError:
        git_tag = ""
if git_tag:
    git_tag_phrase = f'showing release <a href="https://gitlab.esss.lu.se/ess-bp/ess-lattice/tree/{git_tag}">{git_tag}</a>,'
else:
    git_tag_phrase = ""

tmp_index = open(os.path.join(base, "index.html.tmp"), "r").read()
open(os.path.join(base, "index.html"), "w").write(tmp_index.format(**locals()))

print(git_branch, git_hash, git_tag)
for sector in ["LEBT", "RFQ", "MEBT", "DTL", "SPK", "MBL", "HBL", "HEBT", "A2T", "DMPL", "MEBT-A2T"]:
    print(sector)
    current = os.path.join(base, sector)
    makedir(current)
    if sector in ["MEBT-A2T"]:
        figsize = (20, 10)
    else:
        figsize = (15, 10)

    tbl = TraceWin.partran(f"{sector}.out")
    # Plot Beam size
    plt.figure(figsize=figsize)
    plt.plot(tbl["z(m)"], tbl["SizeX"], "r", label="X")
    plt.plot(tbl["z(m)"], tbl["SizeY"], "b", label="Y")
    plt.xlabel("z [m]")
    plt.ylabel(r"$\sigma$ [mm]")
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(current, "beamsize.png"))
    plt.close()

    # Plot Beam Emittances
    plt.figure(figsize=figsize)
    plt.plot(tbl["z(m)"], tbl["ex"], "r", label=r"$\epsilon_x$")
    plt.plot(tbl["z(m)"], tbl["ey"], "b", label=r"$\epsilon_y$")
    plt.xlabel("z [m]")
    plt.ylabel(r"$\epsilon$ [mm mrad]")
    plt.legend()
    plt.grid()
    plt.savefig(os.path.join(current, "emittance.png"))
    plt.close()

    # Plot Beam energy
    plt.figure(figsize=figsize)
    plt.plot(tbl["z(m)"], tbl["gama-1"] * 938.272029, "r")
    plt.xlabel("z [m]")
    plt.ylabel("Energy [MeV]")
    plt.grid()
    plt.savefig(os.path.join(current, "energy.png"))
    plt.close()
