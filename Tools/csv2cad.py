import csv


def run(args):
    with open(args.input, "r") as csvin, open(args.output, "w") as csvout:
        reader = csv.DictReader(csvin, delimiter=",")
        writer = csv.writer(csvout, delimiter=",")
        for row in reader:
            name = row["essname"]
            if name:
                x = float(row[f"TCS:{args.loc}:x [mm]"]) / 1000
                y = float(row[f"TCS:{args.loc}:y [mm]"]) / 1000 * -1
                z = float(row[f"TCS:{args.loc}:z [mm]"]) / 1000

                writer.writerow([f"ESS Lattice_{args.branch}\\{name}\\Origin\\X", z])
                writer.writerow([f"ESS Lattice_{args.branch}\\{name}\\Origin\\Y", x])
                writer.writerow([f"ESS Lattice_{args.branch}\\{name}\\Origin\\Z", y])
        print(args.output, "created")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Convert CSV to CSV for CAD skeleton comparison..")
    parser.add_argument("-i", dest="input", default="CSV_Tables/elements.csv")
    parser.add_argument("-o", dest="output", default="elements.csv")
    parser.add_argument("-b", dest="branch", default="next")
    parser.add_argument("-l", dest="loc", default="m")
    args = parser.parse_args()

    run(args)
