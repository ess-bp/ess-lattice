from __future__ import print_function, division

# Assumes that the cone length is in mm (no decimals)

# Cone is a total of 198 mm long including electrode and RFQ end plate,
# with a minimum aperture of 7mm and maximum of 35mm radii
# before the cone starts there is a 14 mm gap from the valve
# Total 210 mm from valve to RFQ inner wall
# 2.87 mm of first gap is inside the field map so subtracted here

# Field map length
len_fmap = 0.103362 + 0.023
# Length of the end part (electrode etc)
len_end = 0.023

# The length of the cone only (excluding part with max aperture len_as_max)
len_cone = 0.175
# Length where cone has maximum radius
len_as_max = 0.0
# cone has minimum length during this distance
len_as_min = 0.000

# Maximum aperture at beginning of cone
rmax = 0.07
# Minimum aperture at end of cone
rmin = 0.007

_range = int(len_cone / 0.001 - len_as_min - 1)

# support a cut of the cone:
len_missing = len_cone + len_as_max + len_end - len_fmap
if len_missing < 0:
    raise ValueError("Length of field map must be smaller or equal to length of cone+end")
if len_missing > len_cone + len_as_max:
    raise ValueError("You are cutting more than the entire cone")

output = []
# First number of field map must hold the number of points in the field map:
count = 0

if len_missing == 0:
    output.append([0.0, rmax])
    count += 1

for i in range(_range):
    curr_len = len_as_max + i * 0.001
    if curr_len > len_missing:
        output.append([curr_len, rmax - (rmax - rmin) * (i / _range)])
        count += 1

output.append([len_as_max + len_cone - len_as_min, rmin])
count += 1


#   electrode and end shape..
# output.append([len_as_max+len_cone      ,0.020])
# output.append([len_as_max+len_cone+0.002,0.018])
# output.append([len_as_max+len_cone+0.003,0.020])
# count+=3
#   gap
# output.append([len_as_max+len_cone+0.005,0.05])
# count+=1
#
#   very approximate.. important is that it ends at 7 mm again
# for i in range(15):
#    output.append([len_as_max+len_cone+0.0062+0.001*i,0.014-i*0.0005])
#    count+=1
#
# output.append([len_as_max+len_cone+len_end,0.007])
# count+=1

# Print the output to screen
print(count)
for o in output:
    print("{:.5f} {}".format(o[0] - len_missing, o[1]))
