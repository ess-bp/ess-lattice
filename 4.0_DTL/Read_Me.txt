Please add your files here, with new names, and add to the logbook what has changed.

=================================================

 The lattice from the RFQ exit to the target as of 2014-07-03

 - The RFQ output distribution is from the RFQ ver in Fall 2013 (Oct?):

   /Dropbox/ESS_Linac/3.0_MEBT/Beam_Physics/dist.rfq_rfq.2013.10_4sig.1M.dst

 - Versions of each sections:

   * MEBT: 2014.v1
   * DTL : v86
   * SC  : Optimus+ (with field maps Spoke/MB/HB_F2F.edz)
   * HEBT: raster27

=================================================


1) 4 particles are filtered from the MEBT output distribution because they give losses even in the ideal lattice.

2) I leave even the MEBT used to get result in the LINAC'14 paper: to simulate the "beam output error range" (dx, dy, dx', etc. etc.) the best thing is to use the MEBT and to use the first BMP in the first tank of the DTL linked to the last steerers in the MEBT.

3) Could be useful to fix the maximum value in the MEBT to 12G.m to reduce the losses.
1) 4 particles are filtered from the MEBT output distribution because they give losses even in the ideal lattice.

2) I leave even the MEBT used to get result in the LINAC'14 paper: to simulate the "beam output error range" (dx, dy, dx', etc. etc.) the best thing is to use the MEBT and to use the first BMP in the first tank of the DTL linked to the last steerers in the MEBT.

3) Could be useful to fix the maximum value in the MEBT to 12G.m to reduce the losses.


### Drift Tube Length

The file drift-tube-lengths_dtl.v85.txt has a list of all lengths of drift tubes. This is for example useful if you need to know the losses in W/m instead of W/element.
  - 1st column is total length of half cell element
  - 2nd column is tube length
  - 3rd is gap length

