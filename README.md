# This repository contains the baseline ESS lattices from Beam Physics.

### Current release: 2023.v1

See the [Configuration Change Request](https://chess.esss.lu.se/enovia/link/ESS-5045802/21308.51166.50856.56767/valid) for a description.

### Data tables

The data in the lattice files are represented in more convenient CSV and Markdown formats. CSV can be opened in Excel, and Markdown tables can be read directly in the browser (click the link)

  * [Beam parameters at interfaces markdown](MD_Tables/BeamParameters.md)
  * [Elements CSV](CSV_Tables/elements.csv)
  * [Elements markdown](MD_Tables/elements.md)
  * [Sections CSV](CSV_Tables/sections.csv)

The simulated beam parameters of the Baseline can be found on our web page [http://ess-bp.pages.esss.lu.se/ess-lattice/](http://ess-bp.pages.esss.lu.se/ess-lattice/).

We urge that anyone who are not familiar with TraceWin syntax use either the CSV tables or the [Synoptic Viewer](https://confluence.esss.lu.se/ALSV/) to find the relevant information. There are no difference between the TraceWin files, the CSV tables and the Synoptic viewer, they are simply different representations of the same information.

## Which version should I use?

Rule no. 1: Ask a member of the beam physics work package.

The baseline branch holds the latest approved lattice. Each approved lattice is tagged, so when you take a version remember which tag you used. The baseline branch is under change control, as decided in the [18th TB](https://indico.esss.lu.se/event/499/).

The next branch holds changes by beam physics which are expected to be correct and ready. However, rigorous testing and checks have *not* been performed on this branch.

If you want to copy/clone from git, you should get your files from the [gitlab repository](https://gitlab.esss.lu.se/ess-bp/ess-lattice) which is the official storage. Use of any other branches is at your own discretion. A good practice is to check who has put recent developments on the given branch and ask that person if it makes sense to select that version for your intended use.

## Instrumentation numbering system

We use 5-digit numbers XYZnn to index an instrumentation. X identifies the section, Y physics, Z family and the last two as index.

  * X: Section
    * 1 LEBT
    * 2 MEBT
    * 3 DTL
    * 4 SPK
    * 5 MB
    * 6 HB
    * 7 HEBT
    * 8 A2T
    * 9 Dumpline
  * Y: Physics
    * 0 (reserved) ... currently used for match_fam commands
    * 1 RF cavity tuning/phase scan
    * 2 Twiss
    * 3 Beam size
    * 4 Special linear (for commands used in HEBT, diag_waist, diag_set_matrix, diag_set_achromat, ...)
    * 5 Special multi-particle (multi-particle optimization such as emittance, transmission, loss, and etc, in case we do.)
    * 6 Current
    * 7 Aperture monitor,  
    * 8 () Reserved
    * 9 Centroid
  * Z: Family: An extra index to distinguish cases with the same X and Y. (To distinguish planes, global/local, and etc)
    * 0-3 () Reserved
    * 4 Aperture monitor beam size in x and y. 
    * 5-9 () Reserved


## Comments to Instrument elements and commands in TraceWin

TRACWIN\_CMD a b c ... ; #NAME MODEL[d\_z\_mm=?? bla=??]#

Where:
  * NAME is BPM, BCM, ..
  * MODEL is BPM1, BCM3 (DOORS ID)
  * optional space delimited parameters can follow in key=value fashion (no spaces for key or value)
    * d\_z\_mm=x in used in the CSV generator to move the position x mm downstream. This is used for overlapping elements


## Information for developers

Please add the git hooks from Tools/git-hooks to your repository clone:

```
ln -s ../../Tools/git-hooks/pre-push .git/hooks/pre-push
```

It is also recommended to add the pre-commit hook:

```
pre-commit install
```

The baseline branch, as well as the next branch, are blocked from direct push. If you are making changes, place the
changes on a new branch and submit a merge request from your branch into next once you are finished with your changes.

Do note that several developers chose to have a branch next-<name> which contains next plus their changes. Please do not use
those without checking with the ''branch owner'' first.

If you own one of these branches, please remember that it is up to you to pull in changes from next and keep it up to date,
before you submit new merge requests from your branch.

## Information about the lattice versions

### 2023.v1

This is a minor update compared to the previous baseline.

- [A2T] Q5 moved 52 mm downstream, Q6 moved 331 mm downstream
- DMPL diagnostic locations update
- Quads rename
- DTL1 BCM
- A2T BPM position update
- Adding beam parameters table
- Update b3 component of MEBT steerer
- DmpL quad max strength update (in ADJUST commands)
- using new tracewin cli arguments
- New rastering element
- Adding new command FREQ\_DIAG
- Hide Show BLM button from synoptic
- New parameter in FIELD\_MAP command
- Split HEBT wirescanner stations
- LEBT solenoids, MEBT FBCM location
- add field maps for MEBT bunchers
- ESS Names directly in lattice
- Update solenoid field-maps

### 2019.v1

This is a minor update compared to the previous baseline, but contains significant improvements for the synptic representation.

Changes include:
 - Vacuum valves, BLM’s added to synoptic
 - Several changes to LEBT layout
   - ISrc-LEBT mechanical interface updated based on ESS-0036704.
   - Drift lengths adjusted according to ESS-0037491.
   - 2nd BCM moved to the RFQ interface.
   - Collimator marker moved to the minimum aperture position.
   - Chopper aperture radius is updated from 35 to 40 mm
 - Minor updates to chopper, chopper dump, and diag devices in MEBT
 - MBL LBM has been deferred from construction budget (CR 11.00210.1)
 - HBL/MBL aperture fields now included in lattice
 - Tgt Img moved from PBIP to downstream side of PBW
 - DTL stem cell positions now accurately defined following ESS-0103355

### 2017.v1

This is the first baseline which is considered to be largely in sync with the 3D model for the major part of the linac.
A lot of changes to e.g. diagnostics in the LWU have been done to reflect what is in the 3D model.
This is the first version which contains a functional synoptic viewer, automatically deployed to Confluence.

The changes in this baseline release were summarized in three change request at [TB #22](https://indico.esss.lu.se/event/708/),  CR11.00138, CR11.00139, and CR11.00140.
This TB was also where this baseline was approved.
You may also look at merge request !1 for details.

Major changes include:
 - Inclusion of a synoptic viewer
 - Optimizing the BPM and steerer positions in the DTL
 - Changes to the LWU diagnostics to reflect the 3D model
 - LEBT changes to reflect the 3D model updates
 - Updated DMPL to reflect the 3D model, and to optimise optics for the longer 
   drift before the dump
 - Moving the A2T raster components based on latest drawings from manufacturer
 - A2T dipole corrector was moved
 - A2T BPM positions were updated
 - A2T WS was moved

### 2016.v2

For all segments, diagnostics have been added and are for the most part now up to date. Diagnostic and actuator ID numbers are updated according to new specs. Markers are added which defines optical and mechanical interfaces between sections.
No error study was performed on this version (the beam optics is not matched).

- LEBT
 - Updated LEBT according to latest drawings from INFN
 - Adjusted length of cone (240mm -> 210mm)
- MEBT
 - Number of BPMs increased to 7
 - Steerer max value and b3 updated
 - Geometry of chopper and its dump updated 
- DTL
 - Now using absolute phases instead of relative.
- Spokes
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - New LWU apertures (57.1mm)
 - Steerers moved to match drawings
 - New solenoid field maps
- Medium Beta
 - Drifts have changed (shorter LWUs)
 - Field maps updated (aperture for cavities)
 - Steerer max/min updated
 - Steerers moved to match drawings
- High Beta
 - Drifts have changed (shorter LWUs)
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
- Contingency
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
- A2T
 - Drifts have changed (shorter LWUs)
 - Steerer max/min updated
 - Steerers moved to match drawings
 - Introduced displaced apertures in monolith
 - Changed distances up to and including NSW
 - Adjusted monolith drifts
 - Updated BPMs according to PBI report


### FDSL\_2012\_05\_09

The May release was the 2012 baseline. Before that the lattice was changing more rapidly to keep one as baseline.
******
2012.10 from ESS\_Linac/Misc/2012.10.zip
******
Optimus
25 July 2013, Mamad

The Field for the Spokes is from Guillaume Olry
The MB comes from Gabriele Costanza
The HB field comes from Guillaume Devanz.

The E\_acc are, 9, 16.785, 19.943 MV/m respectively.
******

### RFQ2TGT

 - Optional Ver (?) of MEBT 2013 v4 (2013 Oct 15th)
 - Matched to the RFQ of Aug 2013.
 - Matched to DTL v84.
 - 3rd buncher moved to reduce E0TL.

***************

### MEBT 2014.v1

 - This is MEBT 2014.v1 but also includes the following sections:

   * DTL : v86
   * SC  : Optimus+ (with field maps Spoke/MB/HB\_F2F.edz)
   * HEBT: raster27

 - The input is from RFQ of 2013.10.
 - Matching in z is done with a python script.
 - The error are standard tolerances presented in TAC 2014.06.

 - An error study for the MEBT col in Oct 2014 is done with a wrong set of
   dynamic errors for the MEBT buncher cavities. See below.

***************

### 2014\_Baseline/MEBT\_TGT(R29)

MEBT: lat.mebt\_2014.v1\_nom
DTL: DTL\_v86\_presented\_at\_LINAC14
SCL: OptimusPlus\_2014BL\_ModSpkPhase
HEBT: HEBT\_raster29\_TGT with ramped phase advance in the contingency

3D Fieldmaps
