Date		Modifier		What has changed?

2013-Sep-23	Mamad			The folders are created.

2013-Nov-03	Ryoichi			The lattice for the 2013 Oct Baseline (2013.v6) is added.

					    - Nom      : the lattice for the nominal emittances of 0.25 and 0.36.
					    - 0.20.0.29: the lattice re-tuned for smaller emittances of 0.25 and 0.36.
					    - 0.30.0.43: the lattice re-tuned for larger emittances of 0.30 and 0.43.

2014-Jan-16     Ryoichi			Mindor modifications from 2013.v6 ver to 2014.v0 ver:

					    - J-PARC like quads adobted.
					    - The aperture R increased: 15 mm => 18.4 mm, except inside bunchers.
					    - The aperture inside the chopper modified. (Was unnecessary small in H.)
					    - The chopper dump cross section changed from a circle to a square.
					    - Minor adjustments to spaces.

2014-Feb-06	Ryoichi			- Chopper efficiency verified better than 99% for the 2014.v0 lattice.
					  See comments in the file for details.
					- Scrapers added at 3 locations. 3 settings (0%, 50%, 100%) are defined for now.
					  The criteria for the limist remains the same as the last year:
					  1 blade can absorbe particles-above-3-sigma <=> ~0.13% <=> ~12 W
					  (Should be verified with the new set of parameters)

2014-Feb-24	Ryoichi                 - Transmissions and losses for half chopped bunches are simulated: /Beam_Physics/chopper.vs.volt/
                                        - Results are from ~1M particles tracking with no error.
                                        - It looks that the half chopped bunches cause no significant issue (as long as rise/fall is fast enough).
                                        - Scrapers found very helpful.

2014-Apr-25	Ryoichi                 - Small modifications to the lattice to match to the DTL v85 (used to be matched to v84).
                                        - Since the modifications are minor, the file name remains the same (2014.v0_nom).
                                        - Steerers and BPMs are added to all the quads. At this point they are  at the entrace of
                                          a quad but the ideal way may be split the quad and locate them at the center of a quad.
                                          In any case, studies will be performed about the BPM and steerer requirements.
                                        - The commands used of the end-to-end and the chopper studies are left. The number there
                                          are not necessarily the "standard" values yet.

2014-May-9	Bustinduy               - branch: Space allowed for the buncher is increased to allow a 190mm wide buncher. 

2014-Jun-8	Ryoichi			- The new version 2014.v1 is uploaded. The differences from 2014.v0 are

					  * Buncher length extended from 180 mm to 190 mm.
					  * Matched to a new DTL (v86).
					  * Matching to the DTL done with an external python script.
					    (Only for z at the moment. Will be extended to 3D.)
					  * "THIN_STEERING" replaced "STEERER".

2014-Sep-30	Ryoichi                 - The nominal lattice 2014.v1 is slightly modified.

                                          * Splitting a quad into two pieces was found to cause an issue in error studies,
                                            so all the quads are merged into one piece again and "ADJUST_STEERER" and "STEERER"
                                            are used again for the trajectory correction.
                                          * Various scraper settings (4, 3.5, 3, 2.5, 2, sigma's)  were calculated and added as comments.

2014-Oct-15	Ryoichi                 - Beam_Physics folder organized.
